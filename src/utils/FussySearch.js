class FussySearch {
    constructor() {
        this.search = this.search.bind(this);
        this.setDictionary = this.setDictionary.bind(this);
    }

    setDictionary(data) {
        this.dictionary = data
            .reduce((acc, str) => {
                const wordsArr = str.split(' ').map((word) => {
                    return {text: word, codes: this._getKeyCodes(word)};
                });
                return {...acc, [str]: wordsArr};
            }, {});
    }

    _getKeyCodes(word) {
        return word.split('').map((char) => CODE_KEYS[char] ? CODE_KEYS[char] : char);
    }

    _getRangePhrase(sourceWords = [], searchWords = []) {
        if (!sourceWords.length) {
            return searchWords.length ? (searchWords.reduce((acc, {text}) => acc + text.length, 0) * 2 * 100) : 0;
        }
        if (!searchWords.length) {
            return sourceWords.reduce((acc, {text}) => acc + text.length, 0) * 2 * 100;
        }

        let result = 0;
        searchWords.forEach((searchWord, searchIndex) => {
            let minRangeWord = Number.MAX_SAFE_INTEGER;
            let minIndex = 0;
            sourceWords.forEach((sourceWord, sourceIndex) => {
                const currentRangeWord = this._getRangeWord(sourceWord, searchWord);

                if (currentRangeWord < minRangeWord) {
                    minRangeWord = currentRangeWord;
                    minIndex = sourceIndex;
                }
            });
            result += minRangeWord * 100 + (Math.abs(searchIndex - minIndex) / 10);
        });
        return result;
    }

    _levenshteinDistance(source, target, fullWord) {
        const distance = [];
        if (!source.text) {
            return target.text ? (target.text.length * 2) : 0;
        }
        if (!target.text) return source.text.length * 2;
        const n = source.text.length;
        const m = target.text.length;

        for (let i = m + 1; i >= 0; i--) distance[i] = [];

        for (let j = m; j >= 0; j--) distance[0][j] = j * 2;

        let currentRow = 0;
        for (let i = 1; i <= n; ++i) {
            currentRow = i % 3;
            const previousRow = (i - 1) % 3;
            distance[currentRow][0] = i * 2;
            for (let j = 1; j <= m; j++) {
                distance[currentRow][j] = Math.min(
                    Math.min(
                    distance[previousRow][j] + ((!fullWord && i === n) ? 2 - 1 : 2),
                        distance[currentRow][j - 1] + ((!fullWord && i === n) ? 2 - 1 : 2),
                    ),
                    distance[previousRow][j - 1] + this._costDistanceSymbol(source, i - 1, target, j - 1));

                if (i > 1 && j > 1 && source.text[i - 1] === target.text[j - 2]
                    && source.text[i - 2] === target.text[j - 1]) {
                    distance[currentRow][j] = Math.min(distance[currentRow][j], distance[(i - 2) % 3][j - 2] + 2);
                }
            }
        }
        return distance[currentRow][m];
    }

    _costDistanceSymbol(source, sourcePosition, search, searchPosition) {
        if (source.text[sourcePosition] === search.text[searchPosition]) return 0;
        if (source.codes[sourcePosition] && source.codes[sourcePosition] === search.codes[searchPosition]) {
            return 0;
        }

        let resultWeight;
        const nearKeys = DISTANCE_CODE_KEY[source.codes[sourcePosition]];

        if (!nearKeys || !nearKeys.length) {
            resultWeight = 2;
        } else {
            resultWeight = nearKeys.includes(search.codes[searchPosition]) ? 1 : 2;
        }

        const phoneticGroup = PHONETIC_GROUPS.find((group) => group.includes(search.text[searchPosition]));
        if (phoneticGroup) {
            resultWeight = Math.min(resultWeight, phoneticGroup.includes(source.text[sourcePosition]) ? 1 : 2);
        }
        return resultWeight;
    }

    _getRangeWord(source, target, transition) {
        let minDistance = Number.MAX_SAFE_INTEGER;
        const croppedSource = {};
        const length = Math.min(source.text.length, target.text.length + 1);
        for (let i = 0; i <= source.text.length - length; i++) {
            croppedSource.text = source.text.substring(i, length);
            croppedSource.codes = source.codes.slice(i, length);
            const fullWord = croppedSource.text.length === source.text.length;
            const levenshteinResult = this._levenshteinDistance(croppedSource, target, fullWord, transition);
            const currentDistance = levenshteinResult + (i * 2 / 10);
            minDistance = Math.min(minDistance, currentDistance);
        }
        return minDistance;
    }

    search(searchStr) {
        const { matches, restPhrases } = Object.keys(this.dictionary).reduce((acc, phrase) => {
            phrase.includes(searchStr) ? acc.matches.push(phrase) : acc.restPhrases.push(phrase);
            return acc;
        }, { matches: [], restPhrases: [] });

        if (searchStr.length < 3) {
            return matches;
        }

        const searchWords = searchStr.split(' ').map((word) => {
            return {text: word, codes: this._getKeyCodes(word)};
        });

        let minScore = Number.MAX_SAFE_INTEGER;
        const scoresByPhrase = {};

        restPhrases.forEach((phrase) => {
            const score = this._getRangePhrase(this.dictionary[phrase], searchWords);
            scoresByPhrase[phrase] = score;
            if (score < minScore) minScore = score;
        });

        const fussyResults = Object.keys(scoresByPhrase).filter((phrase) => {
            if (minScore < 70) return scoresByPhrase[phrase] < 140;
            if (minScore > 400) return false;
            return (scoresByPhrase[phrase] - minScore) < (minScore / 3);
        });
        return matches.concat(fussyResults);
    }
}

let singletonFussySearch = null;
if (!singletonFussySearch) {
    singletonFussySearch = new FussySearch();
}

export default singletonFussySearch;

const PHONETIC_GROUPS = ['aeiouy', 'bp', 'ckq', 'dt', 'lr', 'mn', 'gj', 'fpv', 'sxz', 'csz'];

const CODE_KEYS = {
    '`': 192,
    '1': 49,
    '2': 50,
    '3': 51,
    '4': 52,
    '5': 53,
    '6': 54,
    '7': 55,
    '8': 56,
    '9': 57,
    '0': 48,
    '-': 189,
    '=': 187,
    'q': 81,
    'w': 87,
    'e': 69,
    'r': 82,
    't': 84,
    'y': 89,
    'u': 85,
    'i': 73,
    'o': 79,
    'p': 80,
    '[': 219,
    ']': 221,
    'a': 65,
    's': 83,
    'd': 68,
    'f': 70,
    'g': 71,
    'h': 72,
    'j': 74,
    'k': 75,
    'l': 76,
    ';': 186,
    '\'': 222,
    'z': 90,
    'x': 88,
    'c': 67,
    'v': 86,
    'b': 66,
    'n': 78,
    'm': 77,
    ',': 188,
    '.': 190,
    '/': 191,

    '~': 192,
    '!': 49,
    '@': 50,
    '#': 51,
    '$': 52,
    '%': 53,
    '^': 54,
    '&': 55,
    '*': 56,
    '(': 57,
    ')': 48,
    '_': 189,
    '+': 187,

    '{': 219,
    '}': 221,
    ':': 186,
    '"': 222,

    '<': 188,
    '>': 190,
    '?': 191,
};

const DISTANCE_CODE_KEY = {
    192: [49],
    49: [50, 87, 81],
    50: [49, 81, 87, 69, 51],
    51: [50, 87, 69, 82, 52],
    52: [51, 69, 82, 84, 53],
    53: [52, 82, 84, 89, 54],
    54: [53, 84, 89, 85, 55],
    55: [54, 89, 85, 73, 56],
    56: [55, 85, 73, 79, 57],
    57: [56, 73, 79, 80, 48],
    48: [57, 79, 80, 219, 189],
    189: [48, 80, 219, 221, 187],
    187: [189, 219, 221],
    81: [49, 50, 87, 83, 65],
    87: [49, 81, 65, 83, 68, 69, 51, 50],
    69: [50, 87, 83, 68, 70, 82, 52, 51],
    82: [51, 69, 68, 70, 71, 84, 53, 52],
    84: [52, 82, 70, 71, 72, 89, 54, 53],
    89: [53, 84, 71, 72, 74, 85, 55, 54],
    85: [54, 89, 72, 74, 75, 73, 56, 55],
    73: [55, 85, 74, 75, 76, 79, 57, 56],
    79: [56, 73, 75, 76, 186, 80, 48, 57],
    80: [57, 79, 76, 186, 222, 219, 189, 48],
    219: [48, 186, 222, 221, 187, 189],
    221: [189, 219, 187],
    65: [81, 87, 83, 88, 90],
    83: [81, 65, 90, 88, 67, 68, 69, 87, 81],
    68: [87, 83, 88, 67, 86, 70, 82, 69],
    70: [69, 68, 67, 86, 66, 71, 84, 82],
    71: [82, 70, 86, 66, 78, 72, 89, 84],
    72: [84, 71, 66, 78, 77, 74, 85, 89],
    74: [89, 72, 78, 77, 188, 75, 73, 85],
    75: [85, 74, 77, 188, 190, 76, 79, 73],
    76: [73, 75, 188, 190, 191, 186, 80, 79],
    186: [79, 76, 190, 191, 222, 219, 80],
    222: [80, 186, 191, 221, 219],
    90: [65, 83, 88],
    88: [90, 65, 83, 68, 67],
    67: [88, 83, 68, 70, 86],
    86: [67, 68, 70, 71, 66],
    66: [86, 70, 71, 72, 78],
    78: [66, 71, 72, 74, 77],
    77: [78, 72, 74, 75, 188],
    188: [77, 74, 75, 76, 190],
    190: [188, 75, 76, 186, 191],
    191: [190, 76, 186, 222],
};
