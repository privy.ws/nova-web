const BRIDGES = [
    'facebook',
    'hangouts',
    'telegram',
    'whatsapp',
    'instagram',
    'twitter',
    'slack',
];

const getBridgedUserRegex = (userName) => {
    return new RegExp(`^@_${userName}_\\w+:nova.chat$`);
};

export const checkIsBridge = (userId) => {
    let match;
    BRIDGES.forEach((name) => {
        if (userId.includes(name)) match = true;
    });
    return match && userId.includes('bot');
};

export const checkIsDirectChat = (room) => {
    const { currentState: { members }, myUserId } = room;

    const usersCount = Object.keys(members).filter((key) => {
        const { userId, membership } = members[key];

        if (checkIsBridge(userId) || userId === myUserId) {
            return false;
        }
        return membership === 'join';
    });

    return usersCount.length < 2;
};

export const checkIsBridgedChat = (room) => {
    const { myUserId, currentState } = room;
    const { event } = currentState.getStateEvents("m.room.create", "") || {};

    if (!event || !event.sender) return false;

    const myName = myUserId.split(':')[0].substring(1);
    const { sender } = event;
    return getBridgedUserRegex(myName).test(sender) || checkIsBridge(sender);
};

