/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2018 New Vector Ltd
Copyright 2019 Michael Telatynski <7t3chguy@gmail.com>
Copyright 2019 The Matrix.org Foundation C.I.C.
Copyright 2020 Nova Technology Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import React from 'react';
import PropTypes from 'prop-types';
import { EventStatus } from 'matrix-js-sdk';
import dis from 'matrix-react-sdk/src/dispatcher';
import * as sdk from 'matrix-react-sdk/src/index';
import { _t } from 'matrix-react-sdk/src/languageHandler';
import Modal from 'matrix-react-sdk/src/Modal';
import RoomContext from 'matrix-react-sdk/src/contexts/RoomContext';
import { isUrlPermitted } from 'matrix-react-sdk/src/HtmlUtils';
import { isContentActionable, canEditContent } from 'matrix-react-sdk/src/utils/EventUtils';
import { MenuItem } from 'matrix-react-sdk/src/components/structures/ContextMenu';
import SdkConfig from 'matrix-react-sdk/src/SdkConfig';
import MessageContextMenu from 'matrix-react-sdk/src/components/views/context_menus/MessageContextMenu';
import * as emailService from '../../../../../utils/emailService';

function canCancel(eventStatus) {
    return eventStatus === EventStatus.QUEUED || eventStatus === EventStatus.NOT_SENT;
}

export default class NovaMessageContextMenu extends MessageContextMenu {
    static propTypes = {
        ...super.propTypes,
        onReactContextMenu: PropTypes.func.isRequired,
    };

    static contextType = RoomContext;

    constructor(props, context) {
        super(props, context);
        this.state = {
            ...this.state,
            selectedText: '',
        };
    }

    componentDidMount() {
        if (window.getSelection && window.getSelection().toString()) {
            this.setState({ selectedText: window.getSelection().toString() });
        }

        this.props.mxEvent.on("Event.decrypted", this.onDecrypted);
        this.props.mxEvent.on("Event.beforeRedaction", this.onBeforeRedaction);
        super.componentDidMount();
    }

    componentWillUnmount() {
        this.props.mxEvent.removeListener("Event.decrypted", this.onDecrypted);
        this.props.mxEvent.removeListener("Event.beforeRedaction", this.onBeforeRedaction);
        super.componentWillUnmount();
    }

    onDecrypted = () => {
        // When an event decrypts, it is likely to change the set of available
        // actions, so we force an update to check again.
        this.forceUpdate();
    };

    onBeforeRedaction = () => {
        // When an event is redacted, we can't edit it so update the available actions.
        this.forceUpdate();
    };

    onSendEmailClick = () => {
        const { mxEvent } = this.props;
        const email = localStorage.getItem('nova_user_email');
        if (!email) {
            const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");
            Modal.createTrackedDialog('Invalid email address', '', ErrorDialog, {
                title: _t("Invalid Email Address"),
                description: _t("Enter your email address in settings to enable this feature"),
            });
            this.closeMenu();
            return;
        }
        const url = SdkConfig.get().email_sending_endpoint_url;

        const encrypted = mxEvent.getType() !== mxEvent.getWireType();
        // FIXME: _clearEvent is private
        const data = encrypted ? { ...mxEvent.event, content: mxEvent._clearEvent.content } : { ...mxEvent.event };
        emailService.sendEmail(url, email, data);
        dis.dispatch({ action: 'track_event', message: 'Send message by email' });
        this.closeMenu();
    }

    onReplyClick = (ev) => {
        dis.dispatch({
            action: 'reply_to_event',
            event: this.props.mxEvent,
        });
        this.closeMenu();
    }

    onEditClick = (ev) => {
        dis.dispatch({
            action: 'edit_event',
            event: this.props.mxEvent,
        });
        this.closeMenu();
    };

    onSelectedCopyClick = (ev) => {
        const { selectedText }= this.state;
        console.log(selectedText);
        navigator.clipboard.writeText(selectedText);

        this.closeMenu();
    };

    closeMenu = () => {
        if (this.props.onFinished) this.props.onFinished();
    }

    render() {
        const mxEvent = this.props.mxEvent;
        const eventStatus = mxEvent.status;
        const editStatus = mxEvent.replacingEvent() && mxEvent.replacingEvent().status;
        const redactStatus = mxEvent.localRedactionEvent() && mxEvent.localRedactionEvent().status;
        const unsentReactionsCount = this._getUnsentReactions().length;
        const pendingReactionsCount = this._getPendingReactions().length;
        const allowCancel = canCancel(mxEvent.status) ||
            canCancel(editStatus) ||
            canCancel(redactStatus) ||
            pendingReactionsCount !== 0;
        let resendButton;
        let resendEditButton;
        let resendReactionsButton;
        let resendRedactionButton;
        let redactButton;
        let cancelButton;
        let forwardButton;
        let pinButton;
        let viewClearSourceButton;
        let unhidePreviewButton;
        let externalURLButton;
        let collapseReplyThread;
        let replyButton;
        let reactButton;
        // status is SENT before remote-echo, null after
        const isSent = !eventStatus || eventStatus === EventStatus.SENT;
        if (!mxEvent.isRedacted()) {
            if (eventStatus === EventStatus.NOT_SENT) {
                resendButton = (
                    <MenuItem className="mx_MessageContextMenu_field" onClick={this.onResendClick}>
                        { _t('Resend') }
                    </MenuItem>
                );
            }

            if (editStatus === EventStatus.NOT_SENT) {
                resendEditButton = (
                    <MenuItem className="mx_MessageContextMenu_field" onClick={this.onResendEditClick}>
                        { _t('Resend edit') }
                    </MenuItem>
                );
            }

            if (unsentReactionsCount !== 0) {
                resendReactionsButton = (
                    <MenuItem className="mx_MessageContextMenu_field" onClick={this.onResendReactionsClick}>
                        { _t('Resend %(unsentCount)s reaction(s)', {unsentCount: unsentReactionsCount}) }
                    </MenuItem>
                );
            }
        }

        if (redactStatus === EventStatus.NOT_SENT) {
            resendRedactionButton = (
                <MenuItem className="mx_MessageContextMenu_field" onClick={this.onResendRedactionClick}>
                    { _t('Resend removal') }
                </MenuItem>
            );
        }

        if (isSent && this.state.canRedact) {
            redactButton = (
                <MenuItem className="mx_MessageContextMenu_field" onClick={this.onRedactClick}>
                    { _t('Remove') }
                </MenuItem>
            );
        }

        if (allowCancel) {
            cancelButton = (
                <MenuItem className="mx_MessageContextMenu_field" onClick={this.onCancelSendClick}>
                    { _t('Cancel Sending') }
                </MenuItem>
            );
        }

        if (isContentActionable(mxEvent)) {
            forwardButton = (
                <MenuItem className="mx_MessageContextMenu_field" onClick={this.onForwardClick}>
                    { _t('Forward Message') }
                </MenuItem>
            );

            if (this.state.canPin) {
                pinButton = (
                    <MenuItem className="mx_MessageContextMenu_field" onClick={this.onPinClick}>
                        { this._isPinned() ? _t('Unpin Message') : _t('Pin Message') }
                    </MenuItem>
                );
            }

            if (this.context.canReact) {
                reactButton = (<MenuItem className="mx_MessageContextMenu_field" onClick={this.props.onReactContextMenu}>
                    { _t('Emoji Reaction') }
                </MenuItem>);
            }

            if (this.context.canReply) {
                replyButton = (
                    <MenuItem className="mx_MessageContextMenu_field" onClick={this.onReplyClick}>
                        { _t('Reply') }
                    </MenuItem>
                );
            }
        }

        const viewSourceButton = (
            <MenuItem className="mx_MessageContextMenu_field" onClick={this.onViewSourceClick}>
                { _t('View Source') }
            </MenuItem>
        );

        if (mxEvent.getType() !== mxEvent.getWireType()) {
            viewClearSourceButton = (
                <MenuItem className="mx_MessageContextMenu_field" onClick={this.onViewClearSourceClick}>
                    { _t('View Decrypted Source') }
                </MenuItem>
            );
        }

        if (this.props.eventTileOps) {
            if (this.props.eventTileOps.isWidgetHidden()) {
                unhidePreviewButton = (
                    <MenuItem className="mx_MessageContextMenu_field" onClick={this.onUnhidePreviewClick}>
                        { _t('Unhide Preview') }
                    </MenuItem>
                );
            }
        }

        // Bridges can provide a 'external_url' to link back to the source.
        if (
            typeof(mxEvent.event.content.external_url) === "string" &&
            isUrlPermitted(mxEvent.event.content.external_url)
        ) {
            externalURLButton = (
                <MenuItem
                    element="a"
                    className="mx_MessageContextMenu_field"
                    target="_blank"
                    rel="noreferrer noopener"
                    onClick={this.closeMenu}
                    href={mxEvent.event.content.external_url}
                >
                    { _t('View Message in Slack') }
                </MenuItem>
            );
        }

        if (this.props.collapseReplyThread) {
            collapseReplyThread = (
                <MenuItem className="mx_MessageContextMenu_field" onClick={this.onCollapseReplyThreadClick}>
                    { _t('Collapse Reply Thread') }
                </MenuItem>
            );
        }

        let e2eInfo;
        if (this.props.e2eInfoCallback) {
            e2eInfo = (
                <MenuItem className="mx_MessageContextMenu_field" onClick={this.e2eInfoClicked}>
                    { _t('End-to-end encryption information') }
                </MenuItem>
            );
        }

        let sendEmailButton;
        if (SdkConfig.get().email_sending_endpoint_url) {
            sendEmailButton = (
                <MenuItem className="mx_MessageContextMenu_field" onClick={this.onSendEmailClick} >
                    { _t('Send as email to myself') }
                </MenuItem>
            );
        }

        let editButton;
        if (canEditContent(this.props.mxEvent)) {
            editButton = (
                <MenuItem className="mx_MessageContextMenu_field" onClick={this.onEditClick}>
                    { _t('Edit') }
                </MenuItem>
            );
        }

        let copyButton;
        if (window.getSelection().toString()) {
            copyButton = (
                <MenuItem className="mx_MessageContextMenu_field" onClick={this.onSelectedCopyClick}>
                    { _t('Copy Selected Text') }
                </MenuItem>
            );
        }

        return (
            <div className="mx_MessageContextMenu">
                { cancelButton }
                { replyButton }
                { reactButton }
                { editButton }
                { copyButton }
                { forwardButton }
                { redactButton }
                { resendButton }
                { resendEditButton }
                { resendReactionsButton }
                { resendRedactionButton }
                { pinButton }
                { viewSourceButton }
                { viewClearSourceButton }
                { unhidePreviewButton }
                { externalURLButton }
                { collapseReplyThread }
                { e2eInfo }
                { sendEmailButton }
            </div>
        );
    }
}
