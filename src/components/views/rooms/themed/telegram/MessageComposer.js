/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2017, 2018 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
import React, {createRef} from 'react';
import PropTypes from 'prop-types';
import { _t } from 'matrix-react-sdk/src/languageHandler';
import {MatrixClientPeg} from 'matrix-react-sdk/src/MatrixClientPeg';
import * as sdk from 'matrix-react-sdk/src/index';
import dis from 'matrix-react-sdk/src/dispatcher';
import Stickerpicker from 'matrix-react-sdk/src/components/views/rooms/Stickerpicker';
import { makeRoomPermalink } from 'matrix-react-sdk/src/utils/permalinks/Permalinks';
import ContentMessages from 'matrix-react-sdk/src/ContentMessages';
import MessageComposer from "matrix-react-sdk/src/components/views/rooms/MessageComposer";

class UploadButton extends React.Component {
    static propTypes = {
        roomId: PropTypes.string.isRequired,
    }

    constructor(props) {
        super(props);
        this.onUploadClick = this.onUploadClick.bind(this);
        this.onUploadFileInputChange = this.onUploadFileInputChange.bind(this);

        this._uploadInput = createRef();
        this._dispatcherRef = dis.register(this.onAction);
    }

    componentWillUnmount() {
        dis.unregister(this._dispatcherRef);
    }

    onAction = payload => {
        if (payload.action === "upload_file") {
            this.onUploadClick();
        }
    };

    onUploadClick(ev) {
        if (MatrixClientPeg.get().isGuest()) {
            dis.dispatch({action: 'require_registration'});
            return;
        }
        this._uploadInput.current.click();
    }

    onUploadFileInputChange(ev) {
        if (ev.target.files.length === 0) return;

        // take a copy so we can safely reset the value of the form control
        // (Note it is a FileList: we can't use slice or sensible iteration).
        const tfiles = [];
        for (let i = 0; i < ev.target.files.length; ++i) {
            tfiles.push(ev.target.files[i]);
        }

        ContentMessages.sharedInstance().sendContentListToRoom(
            tfiles, this.props.roomId, MatrixClientPeg.get(),
        );

        // This is the onChange handler for a file form control, but we're
        // not keeping any state, so reset the value of the form control
        // to empty.
        // NB. we need to set 'value': the 'files' property is immutable.
        ev.target.value = '';
    }

    render() {
        const uploadInputStyle = {display: 'none'};
        const AccessibleButton = sdk.getComponent('elements.AccessibleButton');
        return (
            <AccessibleButton className="mx_MessageComposer_button mx_MessageComposer_upload"
                              onClick={this.onUploadClick}
                              title={_t('Upload file')}
            >
                <input
                    ref={this._uploadInput}
                    type="file"
                    style={uploadInputStyle}
                    multiple
                    onChange={this.onUploadFileInputChange}
                />
            </AccessibleButton>
        );
    }
}

export default class NovaMessageComposer extends MessageComposer {
    constructor(props) {
        super(props);
        this.onInputStateChanged = this.onInputStateChanged.bind(this);
        this._onRoomStateEvents = this._onRoomStateEvents.bind(this);
        this._onRoomViewStoreUpdate = this._onRoomViewStoreUpdate.bind(this);
        this._onTombstoneClick = this._onTombstoneClick.bind(this);
        this.renderPlaceholderText = this.renderPlaceholderText.bind(this);

        this.state = {
            ...this.state,
        };
    }

    render() {
        const E2EIcon = sdk.getComponent('rooms.E2EIcon');
        const controls = [
            // this.state.me ? <ComposerAvatar key="controls_avatar" me={this.state.me} /> : null,
            this.props.e2eStatus ?
                <E2EIcon
                    key="e2eIcon"
                    status={this.props.e2eStatus}
                    isBridged={this.props.isBridged}
                    className="mx_MessageComposer_e2eIcon"
                /> :
                null,
        ];

        if (!this.state.tombstone && this.state.canSendMessages) {
            // This also currently includes the call buttons. Really we should
            // check separately for whether we can call, but this is slightly
            // complex because of conference calls.

            const SendMessageComposer = sdk.getComponent("rooms.SendMessageComposer");

            controls.push(
                <SendMessageComposer
                    ref={(c) => this.messageComposerInput = c}
                    key="controls_input"
                    room={this.props.room}
                    jumpToBottom={this.props.jumpToBottom}
                    placeholder={this.renderPlaceholderText()}
                    permalinkCreator={this.props.permalinkCreator} />,
                <Stickerpicker key="stickerpicker_controls_button" room={this.props.room} />,
                <UploadButton key="controls_upload" roomId={this.props.room.roomId} />,
                // callInProgress ? <HangupButton key="controls_hangup" roomId={this.props.room.roomId} /> : null,
                // callInProgress ? null : <CallButton key="controls_call" roomId={this.props.room.roomId} />,
                // callInProgress ? null : <VideoCallButton key="controls_videocall" roomId={this.props.room.roomId} />,
            );
        } else if (this.state.tombstone) {
            const replacementRoomId = this.state.tombstone.getContent()['replacement_room'];

            const continuesLink = replacementRoomId ? (
                <a href={makeRoomPermalink(replacementRoomId)}
                   className="mx_MessageComposer_roomReplaced_link"
                   onClick={this._onTombstoneClick}
                >
                    {_t("The conversation continues here.")}
                </a>
            ) : '';

            controls.push(<div className="mx_MessageComposer_replaced_wrapper" key="room_replaced">
                <div className="mx_MessageComposer_replaced_valign">
                    <img className="mx_MessageComposer_roomReplaced_icon" src={require("matrix-react-sdk/res/img/room_replaced.svg")} />
                    <span className="mx_MessageComposer_roomReplaced_header">
                        {_t("This room has been replaced and is no longer active.")}
                    </span><br />
                    { continuesLink }
                </div>
            </div>);
        } else {
            controls.push(
                <div key="controls_error" className="mx_MessageComposer_noperm_error">
                    { _t('You do not have permission to post to this room') }
                </div>,
            );
        }
        const ReplyPreview = sdk.getComponent('rooms.ReplyPreview');
        return (
            <div className="mx_MessageComposer">
                {this.props.permalinkCreator && <ReplyPreview permalinkCreator={this.props.permalinkCreator} />}
                <div className="mx_MessageComposer_wrapper">
                    <div className="mx_MessageComposer_row">
                        { controls }
                    </div>
                </div>
            </div>
        );
    }
}

MessageComposer.propTypes = {
    // js-sdk Room object
    room: PropTypes.object.isRequired,

    // string representing the current voip call state
    callState: PropTypes.string,

    // string representing the current room app drawer state
    showApps: PropTypes.bool,
    isBridged: PropTypes.bool,
};
