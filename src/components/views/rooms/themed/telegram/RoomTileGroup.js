import React from 'react';
import * as sdk from 'matrix-react-sdk/src';
import PropTypes from 'prop-types';
import * as Unread from 'matrix-react-sdk/src/Unread';
import * as RoomNotifs from 'matrix-react-sdk/src/RoomNotifs';
import {Draggable, Droppable} from 'react-beautiful-dnd';
import {Key} from 'matrix-react-sdk/src/Keyboard';
import dis from 'matrix-react-sdk/src/dispatcher';

export default class RoomTileGroup extends React.Component {
    static propTypes = {
        list: PropTypes.arrayOf(PropTypes.object).isRequired,
        label: PropTypes.string,
        addRoomLabel: PropTypes.string,
        children: PropTypes.node,
        order: PropTypes.string,
        draggable: PropTypes.bool,
        pinned: PropTypes.bool,
        isInvite: PropTypes.bool,
        refreshSubList: PropTypes.func,
    }

    static defaultProps = {
        label: '',
        draggable: false,
        pinned: false,
        isInvite: false,
    }

    constructor(props) {
        super(props);
    }

    onRoomTileClick = (roomId, ev) => {
        dis.dispatch({
            action: 'view_room',
            room_id: roomId,
            clear_search: (ev && (ev.key === Key.ENTER || ev.key === Key.SPACE)),
        });
    };

    updateSubListCount = () => {
        if (this.props.refreshSubList) {
            this.props.refreshSubList();
        }
    }

    getRoomTile(room) {
        const RoomTile = sdk.getComponent('rooms.RoomTile');
        return <RoomTile
            room={room}
            key={room.roomId}
            collapsed={this.props.collapsed || false}
            unread={Unread.doesRoomHaveUnreadMessages(room)}
            highlight={this.props.isInvite || RoomNotifs.getUnreadNotificationCount(room, 'highlight') > 0}
            notificationCount={RoomNotifs.getUnreadNotificationCount(room)}
            isInvite={this.props.isInvite}
            refreshSubList={this.updateSubListCount}
            onClick={this.onRoomTileClick}
            pinned={this.props.pinned}
        />;
    }

    render() {
        const { draggable, label } = this.props;

        if (!this.props.list || !this.props.list.length) {
            return null;
        }

        const roomList = this.props.list.map((room, index) => {
            return draggable ? (<Draggable
                key={room.roomId}
                draggableId={`room-sub-list-droppable_${room.roomId}`}
                index={index}
                type="draggable-RoomTile"
            >
                {(provided, snapshot) => (
                    <div className={`nv_RoomTileGroup_dragItem ${snapshot.isDragging ? 'nv_RoomTileGroup_dragItemDragging': ''}`}>
                        <div
                            ref={provided.innerRef}
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                        >
                            {this.getRoomTile(room)}
                        </div>
                    </div>
                )}
            </Draggable>) : this.getRoomTile(room);
        });

        return draggable ? (
            <Droppable
                droppableId="room-sub-list-droppable_m.favourite"
                type="draggable-RoomTile"
            >
                {(provided) => (
                    <div
                        style={{height: `${this.props.list.length * this.props.itemHeight}px`, position: 'relative'}}
                        className="nv_RoomTileGroup_dragContainer"
                        ref={provided.innerRef}
                    >
                        {roomList}
                    </div>
                )}
            </Droppable>
        ) : (
            <>
                {label && <div className="nv_RoomTileGroup_label">
                    {label}
                </div>}
                {roomList}
            </>
        );
    }
}
