/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2017 New Vector Ltd
Copyright 2019 Michael Telatynski <7t3chguy@gmail.com>
Copyright 2019, 2020 The Matrix.org Foundation C.I.C.
Copyright 2020 Nova Technology Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import * as sdk from 'matrix-react-sdk/src';
import { _t } from 'matrix-react-sdk/src/languageHandler';
import Modal from 'matrix-react-sdk/src/Modal';

import SettingsStore from 'matrix-react-sdk/src/settings/SettingsStore';
import { MatrixClientPeg } from 'matrix-react-sdk/src/MatrixClientPeg';
import { ALL_RULE_TYPES } from 'matrix-react-sdk/src/mjolnir/BanList';

import MatrixClientContext from 'matrix-react-sdk/src/contexts/MatrixClientContext';
import { E2E_STATE } from 'matrix-react-sdk/src/components/views/rooms/E2EIcon';
import { toRightOf, ContextMenu, aboveLeftOf } from 'matrix-react-sdk/src/components/structures/ContextMenu';
import ReplyThread from 'matrix-react-sdk/src/components/views/elements/ReplyThread';
import EventTile, { getHandlerTile } from 'matrix-react-sdk/src/components/views/rooms/EventTile';
import { boundMethod } from 'autobind-decorator';
const MIN_WIDTH_OFFSET = 200;

const stateEventTileTypes = {
    'm.room.encryption': 'messages.EncryptionEvent',
    // 'm.room.aliases': 'messages.TextualEvent',
    // 'm.room.aliases': 'messages.RoomAliasesEvent', // too complex
    'm.room.canonical_alias': 'messages.TextualEvent',
    'm.room.create': 'messages.RoomCreate',
    'm.room.member': 'messages.TextualEvent',
    'm.room.name': 'messages.TextualEvent',
    'm.room.avatar': 'messages.RoomAvatarEvent',
    'm.room.third_party_invite': 'messages.TextualEvent',
    'm.room.history_visibility': 'messages.TextualEvent',
    'm.room.topic': 'messages.TextualEvent',
    'm.room.power_levels': 'messages.TextualEvent',
    'm.room.pinned_events': 'messages.TextualEvent',
    'm.room.server_acl': 'messages.TextualEvent',
    'im.vector.modular.widgets': 'messages.TextualEvent',
    'm.room.tombstone': 'messages.TextualEvent',
    'm.room.join_rules': 'messages.TextualEvent',
    'm.room.guest_access': 'messages.TextualEvent',
    'm.room.related_groups': 'messages.TextualEvent',
};

// Add all the Mjolnir stuff to the renderer
for (const evType of ALL_RULE_TYPES) {
    stateEventTileTypes[evType] = 'messages.TextualEvent';
}

const MAX_READ_AVATARS = 2;

const checkIsMyMessage = (ev) => {
    const client = MatrixClientPeg.get();
    const me = client && client.getUserId();
    return ev.getSender() === me;
};

export default class NovaEventTile extends EventTile {
    static propTypes = {
        ...super.propTypes,
        directChat: PropTypes.bool,
    }

    static defaultProps = {
        ...super.defaultProps,
        directChat: false,
    }

    static contextType = MatrixClientContext;

    constructor(props, context) {
        super(props, context);

        this.state = {
            ...this.state,
            contextMenuPosition: null,
            contextMenuType: 'main',
        };
    }

    getReadAvatars() {
        // return early if there are no read receipts
        if (!this.props.readReceipts || this.props.readReceipts.length === 0) {
            return null;
        }

        const ReadReceiptMarker = sdk.getComponent('rooms.ReadReceiptMarker');
        const avatars = [];
        const hiddenAvatars = [];

        const receipts = this.props.readReceipts || [];
        for (let i = 0; i < receipts.length; ++i) {
            const receipt = receipts[i];

            let hidden = true;
            if ((i < MAX_READ_AVATARS)) {
                // show hidden markers counter instead last read marker
                hidden = receipts.length > MAX_READ_AVATARS && i === MAX_READ_AVATARS;
            }

            const userId = receipt.userId;
            let readReceiptInfo;

            if (this.props.readReceiptMap) {
                readReceiptInfo = this.props.readReceiptMap[userId];
                if (!readReceiptInfo) {
                    readReceiptInfo = {};
                    this.props.readReceiptMap[userId] = readReceiptInfo;
                }
            }

            // add to the start so the most recent is on the end (ie. ends up rightmost)
            if (hidden) {
                hiddenAvatars.unshift(
                    <ReadReceiptMarker key={userId} member={receipt.roomMember}
                                       fallbackUserId={userId}
                                       leftOffset={0} hidden={false}
                                       readReceiptInfo={readReceiptInfo}
                                       checkUnmounting={this.props.checkUnmounting}
                                       suppressAnimation={this._suppressReadReceiptAnimation}
                                       onClick={this.toggleAllReadAvatars}
                                       timestamp={receipt.ts}
                                       showTwelveHour={this.props.isTwelveHour}
                    />,
                );
            } else {
                avatars.unshift(
                    <ReadReceiptMarker key={userId} member={receipt.roomMember}
                                       fallbackUserId={userId}
                                       leftOffset={0} hidden={false}
                                       readReceiptInfo={readReceiptInfo}
                                       checkUnmounting={this.props.checkUnmounting}
                                       suppressAnimation={this._suppressReadReceiptAnimation}
                                       onClick={this.toggleAllReadAvatars}
                                       timestamp={receipt.ts}
                                       showTwelveHour={this.props.isTwelveHour}
                    />,
                );
            }
        }
        let avatarsCounter;
        if (hiddenAvatars.length) {
            avatarsCounter = (
                <span
                    className="mx_EventTile_readAvatarCounter"
                    onClick={this.toggleAllReadAvatars}
                >
                        +{hiddenAvatars.length}
                    <div
                        className="mx_EventTile_readAvatarTooltip"
                        style={{display: this.state.allReadAvatars ? 'flex': '' }}
                    >
                        {hiddenAvatars}
                    </div>
                </span>
            );
        }

        return <span className="mx_EventTile_readAvatars">
            { avatars }
            { avatarsCounter }
        </span>;
    }

    getContextMenu() {
        const props = {
            onFinished: this.closeMenu,
            mxEvent: this.props.mxEvent,
            reactions: this.state.reactions,
        };

        const { right } = this.state.contextMenuPosition

        const contextMenuBody = this.state.contextMenuType === 'main' ?
            this.getMainContextMenu(props) :
            this.getReactContextMenu(props);

        if (window.innerWidth - right < MIN_WIDTH_OFFSET) {
        return (<ContextMenu
            {...aboveLeftOf(this.state.contextMenuPosition)}
            onFinished={this.closeMenu}
        >
            {contextMenuBody}
        </ContextMenu>);
        }
        return (<ContextMenu
            {...toRightOf(this.state.contextMenuPosition)}
            onFinished={this.closeMenu}
        >
            {contextMenuBody}
        </ContextMenu>)
    }

    getMainContextMenu(props) {
        let e2eInfoCallback = null;

        const onCryptoClick = () => {
            Modal.createTrackedDialogAsync('Encrypted Event Dialog', '',
                import('matrix-react-sdk/src/async-components/views/dialogs/EncryptedEventDialog'),
                {event: props.mxEvent},
            );
        };

        if (props.mxEvent.isEncrypted() && !SettingsStore.getValue("feature_cross_signing")) {
            e2eInfoCallback = onCryptoClick;
        }

        const thread = this.getReplyThread && this.getReplyThread();
        const collapseReplyThread = thread && thread.canCollapse() ? thread.collapse : null;

        const getTileOps = this.getTile && this.getTile().getEventTileOps;
        const eventTileOps = getTileOps && getTileOps();

        const MessageContextMenu = sdk.getComponent('context_menus.MessageContextMenu');
        return (
            <MessageContextMenu
                permalinkCreator={this.props.permalinkCreator}
                eventTileOps={eventTileOps}
                collapseReplyThread={collapseReplyThread}
                e2eInfoCallback={e2eInfoCallback}
                onReactContextMenu={this.onReactContextMenu}
                {...props}
            />
        );
    }

    getReactContextMenu(props) {
        const ReactionPicker = sdk.getComponent('emojipicker.ReactionPicker');
        return <ReactionPicker {...props} />;
    }

    @boundMethod
    onReactContextMenu(e) {
        e.preventDefault();
        this.setState({ contextMenuType: 'react' });
    }

    @boundMethod
    onContextMenu(e) {
        // Prevent the native context menu
        e.preventDefault();
        const newPosition = {
            right: e.clientX,
            top: e.clientY,
            height: 0,
        };
        // TODO change context-menu logic and move menu to parent comp
        // this is a small dirty hack to prevent context-menu flicker
        // it works because setState not sync
        if (this.state.contextMenuPosition) {
            this.closeMenu();
            return;
        }
        this.setState({ contextMenuPosition: newPosition });
    }

    @boundMethod
    closeMenu() {
        this.setState({
            contextMenuPosition: null,
            contextMenuType: 'main',
        });
    }

    render() {
        const MessageTimestamp = sdk.getComponent('messages.MessageTimestamp');
        const SenderProfile = sdk.getComponent('messages.SenderProfile');
        const MemberAvatar = sdk.getComponent('avatars.MemberAvatar');

        const content = this.props.mxEvent.getContent();
        const msgtype = content.msgtype;
        const eventType = this.props.mxEvent.getType();

        // Info messages are basically information about commands processed on a room
        const isBubbleMessage = eventType.startsWith("m.key.verification") ||
            (eventType === "m.room.message" && msgtype && msgtype.startsWith("m.key.verification")) ||
            (eventType === "m.room.encryption");
        let isInfoMessage = (
            !isBubbleMessage && eventType !== 'm.room.message' &&
            eventType !== 'm.sticker' && eventType !== 'm.room.create'
        );

        let tileHandler = getHandlerTile(this.props.mxEvent);
        // If we're showing hidden events in the timeline, we should use the
        // source tile when there's no regular tile for an event and also for
        // replace relations (which otherwise would display as a confusing
        // duplicate of the thing they are replacing).
        const useSource = !tileHandler || this.props.mxEvent.isRelation("m.replace");
        if (useSource && SettingsStore.getValue("showHiddenEventsInTimeline")) {
            tileHandler = "messages.ViewSourceEvent";
            // Reuse info message avatar and sender profile styling
            isInfoMessage = true;
        }

        if (tileHandler === 'messages.TextualEvent') {
            const TextualEvent = sdk.getComponent(tileHandler);
            return <TextualEvent mxEvent={this.props.mxEvent} />;
        }
        // This shouldn't happen: the caller should check we support this type
        // before trying to instantiate us
        if (!tileHandler) {
            const {mxEvent} = this.props;
            console.warn(`Event type not supported: type:${mxEvent.getType()} isState:${mxEvent.isState()}`);
            return <div className="mx_EventTile mx_EventTile_info mx_MNoticeBody">
                <div className="mx_EventTile_line">
                    { _t('This event could not be displayed') }
                </div>
            </div>;
        }
        const EventTileType = sdk.getComponent(tileHandler);

        const isSending = (['sending', 'queued', 'encrypting'].indexOf(this.props.eventSendStatus) !== -1);
        const isRedacted = isMessageEvent(this.props.mxEvent) && this.props.isRedacted;
        const isEncryptionFailure = this.props.mxEvent.isDecryptionFailure();

        const isEditing = !!this.props.editState;
        const myMessage = checkIsMyMessage(this.props.mxEvent);
        const directMessage = false;
        const messageEvent = this.props.mxEvent.getType() === 'm.room.message';
        const stickerEvent = this.props.mxEvent.getType() === 'm.sticker';
        const fileBody = this.props.mxEvent.getContent().msgtype === 'm.file';
        const imageBody = this.props.mxEvent.getContent().msgtype === 'm.image';

        const classes = classNames({
            mx_EventTile_image: imageBody,
            mx_EventTile_file: fileBody,
            mx_EventTile_message: messageEvent,
            mx_EventTile_messageOut: myMessage,
            mx_EventTile_messageIn: !myMessage,
            mx_EventTile_bubbleContainer: isBubbleMessage,
            mx_EventTile_sticker: stickerEvent,
            mx_EventTile_droplet: messageEvent && (this.props.lastInGroup || this.props.last),
            mx_EventTile: true,
            mx_EventTile_isEditing: isEditing,
            mx_EventTile_info: isInfoMessage,
            mx_EventTile_12hr: this.props.isTwelveHour,
            mx_EventTile_encrypting: this.props.eventSendStatus === 'encrypting',
            mx_EventTile_sending: !isEditing && isSending,
            mx_EventTile_notSent: this.props.eventSendStatus === 'not_sent',
            mx_EventTile_highlight: this.props.tileShape === 'notif' ? false : this.shouldHighlight(),
            mx_EventTile_selected: this.props.isSelectedEvent,
            mx_EventTile_continuation: this.props.tileShape ? '' : this.props.continuation,
            mx_EventTile_last: this.props.last,
            mx_EventTile_contextual: this.props.contextual,
            mx_EventTile_actionBarFocused: this.state.actionBarFocused,
            mx_EventTile_verified: !isBubbleMessage && this.state.verified === E2E_STATE.VERIFIED,
            mx_EventTile_unverified: !isBubbleMessage && this.state.verified === E2E_STATE.WARNING,
            mx_EventTile_unknown: !isBubbleMessage && this.state.verified === E2E_STATE.UNKNOWN,
            mx_EventTile_bad: isEncryptionFailure,
            mx_EventTile_emote: msgtype === 'm.emote',
        });

        let permalink = "#";
        if (this.props.permalinkCreator) {
            permalink = this.props.permalinkCreator.forEvent(this.props.mxEvent.getId());
        }

        const readAvatars = this.getReadAvatars();

        let avatar;
        let sender;
        let avatarSize;
        let needsSenderProfile;

        if (this.props.tileShape === "notif") {
            avatarSize = 24;
            needsSenderProfile = true;
        } else if (tileHandler === 'messages.RoomCreate' || isBubbleMessage || isInfoMessage || directMessage) {
            avatarSize = 0;
            needsSenderProfile = false;
        } else if (myMessage && this.props.tileShape !== 'reply' && this.props.tileShape !== 'reply_preview') {
            needsSenderProfile = false;
        } else if (this.props.continuation && this.props.tileShape !== "file_grid") {
            // no avatar or sender profile for continuation messages
            avatarSize = 0;
            needsSenderProfile = false;
        } else {
            avatarSize = 0;
            needsSenderProfile = true;
        }

        if (this.props.lastInGroup || this.props.last) {
            avatarSize = 33;
        }

        if (this.props.mxEvent.sender && avatarSize) {
            avatar = (
                <div className="mx_EventTile_avatar">
                    <MemberAvatar member={this.props.mxEvent.sender}
                                  width={avatarSize} height={avatarSize}
                                  viewUserOnClick={true}
                    />
                </div>
            );
        }

        if (needsSenderProfile) {
                sender = <SenderProfile
                    mxEvent={this.props.mxEvent}
                    onClick={this.onSenderProfileClick}
                    enableFlair={true}
                />;
        }

        const timestamp = this.props.mxEvent.getTs() ?
            <MessageTimestamp showTwelveHour={this.props.isTwelveHour} ts={this.props.mxEvent.getTs()} /> : null;

        const keyRequestHelpText =
            <div className="mx_EventTile_keyRequestInfo_tooltip_contents">
                <p>
                    { this.state.previouslyRequestedKeys ?
                        _t( 'Your key share request has been sent - please check your other sessions ' +
                            'for key share requests.') :
                        _t( 'Key share requests are sent to your other sessions automatically. If you ' +
                            'rejected or dismissed the key share request on your other sessions, click ' +
                            'here to request the keys for this session again.')
                    }
                </p>
                <p>
                    { _t( 'If your other sessions do not have the key for this message you will not ' +
                        'be able to decrypt them.')
                    }
                </p>
            </div>;
        const keyRequestInfoContent = this.state.previouslyRequestedKeys ?
            _t('Key request sent.') :
            _t(
                '<requestLink>Re-request encryption keys</requestLink> from your other sessions.',
                {},
                {'requestLink': (sub) => <a onClick={this.onRequestKeysClick}>{ sub }</a>},
            );

        const TooltipButton = sdk.getComponent('elements.TooltipButton');
        const keyRequestInfo = isEncryptionFailure ?
            <div className="mx_EventTile_keyRequestInfo">
                <span className="mx_EventTile_keyRequestInfo_text">
                    { keyRequestInfoContent }
                </span>
                <TooltipButton helpText={keyRequestHelpText} />
            </div> : null;

        let reactionsRow;
        if (!isRedacted) {
            const ReactionsRow = sdk.getComponent('messages.ReactionsRow');
            reactionsRow = <ReactionsRow
                mxEvent={this.props.mxEvent}
                reactions={this.state.reactions}
            />;
        }

        switch (this.props.tileShape) {
            case 'notif': {
                const room = this.context.getRoom(this.props.mxEvent.getRoomId());
                return (
                    <div className={classes}>
                        <div className="mx_EventTile_roomName">
                            <a href={permalink} onClick={this.onPermalinkClicked}>
                                { room ? room.name : '' }
                            </a>
                        </div>
                        <div className="mx_EventTile_senderDetails">
                            { avatar }
                            <a href={permalink} onClick={this.onPermalinkClicked}>
                                { sender }
                                { timestamp }
                            </a>
                        </div>
                        <div className="mx_EventTile_line">
                            <EventTileType ref={this._tile}
                                           mxEvent={this.props.mxEvent}
                                           highlights={this.props.highlights}
                                           highlightLink={this.props.highlightLink}
                                           showUrlPreview={this.props.showUrlPreview}
                                           onHeightChanged={this.props.onHeightChanged} />
                        </div>
                    </div>
                );
            }
            case 'file_grid': {
                return (
                    <div className={classes}>
                        <div className="mx_EventTile_line">
                            <EventTileType ref={this._tile}
                                           mxEvent={this.props.mxEvent}
                                           highlights={this.props.highlights}
                                           highlightLink={this.props.highlightLink}
                                           showUrlPreview={this.props.showUrlPreview}
                                           tileShape={this.props.tileShape}
                                           onHeightChanged={this.props.onHeightChanged} />
                        </div>
                        <a
                            className="mx_EventTile_senderDetailsLink"
                            href={permalink}
                            onClick={this.onPermalinkClicked}
                        >
                            <div className="mx_EventTile_senderDetails">
                                { sender }
                                { timestamp }
                            </div>
                        </a>
                    </div>
                );
            }

            case 'reply':
            case 'reply_preview': {
                return (
                    <div className={classes}>
                        { sender }
                        <div className="mx_EventTile_reply">
                            <EventTileType ref={this._tile}
                                           mxEvent={this.props.mxEvent}
                                           highlights={this.props.highlights}
                                           highlightLink={this.props.highlightLink}
                                           onHeightChanged={this.props.onHeightChanged}
                                           showUrlPreview={false} />
                        </div>
                    </div>
                );
            }
            // TODO refactor
            default: {
                // hide removed and empty messages
                if (!Object.keys(this.props.mxEvent.getContent()).length) return null;

                const thread = ReplyThread.makeThread(
                    this.props.mxEvent,
                    this.props.onHeightChanged,
                    this.props.permalinkCreator,
                    this._replyThread,
                );

                const isMenuDisplayed = Boolean(this.state.contextMenuPosition);
                let contextMenu;
                if (!isEditing && isMenuDisplayed) {
                    contextMenu = this.getContextMenu();
                }
                // tab-index=-1 to allow it to be focusable but do not add tab stop for it, primarily for screen readers
                return (
                    <div className={classes} tabIndex={-1}>
                        { avatar }
                        <div className="mx_EventTile_line" onContextMenu={this.onContextMenu}>
                            { !isBubbleMessage && needsSenderProfile && this._renderE2EPadlock() }
                            {!this.props.directChat && <div className="mx_SenderProfile_wrapper">{ sender }</div>}
                            { thread }
                            <EventTileType ref={this._tile}
                                           mxEvent={this.props.mxEvent}
                                           // replacingEventId={this.props.replacingEventId}
                                           isBridged={this.props.isBridged}
                                           editState={this.props.editState}
                                           highlights={this.props.highlights}
                                           highlightLink={this.props.highlightLink}
                                           showUrlPreview={this.props.showUrlPreview}
                                           onHeightChanged={this.props.onHeightChanged} />
                                           <div className="mx_EventTile_statusGroup">
                                               {this.props.replacingEventId && <span className="mx_EventTile_edited">edited</span>}
                                               {timestamp}
                                               {readAvatars}
                                           </div>
                            { keyRequestInfo }
                            { contextMenu }
                        </div>
                        { reactionsRow }
                    </div>
                );
            }
        }
    }
}

// XXX this'll eventually be dynamic based on the fields once we have extensible event types
const messageTypes = ['m.room.message', 'm.sticker'];
function isMessageEvent(ev) {
    return (messageTypes.includes(ev.getType()));
}
