/*
Copyright 2019 New Vector Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.
Copyright 2019 Michael Telatynski <7t3chguy@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import React from 'react';
import {_t} from 'matrix-react-sdk/src/languageHandler';
import PropTypes from 'prop-types';
import {MatrixClientPeg} from 'matrix-react-sdk/src/MatrixClientPeg';
import * as sdk from 'matrix-react-sdk/src/index';
import {getThreepidsWithBindStatus} from 'matrix-react-sdk/src/boundThreepids';
import GeneralUserSettingsTab from 'matrix-react-sdk/src/components/views/settings/tabs/user/GeneralUserSettingsTab';
import Field from "matrix-react-sdk/src/components/views/elements/Field";
import AccessibleButton from "matrix-react-sdk/src/components/views/elements/AccessibleButton";
import Modal from "matrix-react-sdk/src/Modal";
import * as Email from "matrix-react-sdk/src/email";

export default class NovaGeneralUserSettingsTab extends GeneralUserSettingsTab {
    static propTypes = {
        closeSettingsFn: PropTypes.func.isRequired,
    };

    static replaces = 'GeneralUserSettingsTab';

    constructor(props) {
        super(props);

        const emailFromStorage = localStorage.getItem('nova_user_email') || '';
        this.state = {
            ...this.state,
            email: emailFromStorage,
            newEmail: emailFromStorage,
        };
    }

    handleEmailChange = (evt) => {
        const { currentTarget: { value } } = evt;
        this.setState({ newEmail: value });
    };

    handleEmailAdd = () => {
        this.setState((prevState) => {
            const { newEmail } = prevState;

            if (!newEmail) return;
            const ErrorDialog = sdk.getComponent("dialogs.ErrorDialog");

            if (!Email.looksValid(newEmail)) {
                Modal.createTrackedDialog('Invalid email address', '', ErrorDialog, {
                    title: _t("Invalid Email Address"),
                    description: _t("This doesn't appear to be a valid email address"),
                });
                return;
            }

            localStorage.setItem('nova_user_email', newEmail);
            return {
                email: newEmail,
            };
        });
    };

    async _getThreepidState() {
        const cli = MatrixClientPeg.get();

        // Check to see if terms need accepting
        this._checkTerms();

        // Need to get 3PIDs generally for Account section and possibly also for
        // Discovery (assuming we have an IS and terms are agreed).
        let threepids = [];
        try {
            threepids = await getThreepidsWithBindStatus(cli);
        } catch (e) {
            const idServerUrl = MatrixClientPeg.get().getIdentityServerUrl();
            console.warn(
                `Unable to reach identity server at ${idServerUrl} to check ` +
                `for 3PIDs bindings in Settings`,
            );
            console.warn(e);
        }
        this.setState({
            emails: threepids.filter((a) => a.medium === 'email'),
            msisdns: threepids.filter((a) => a.medium === 'msisdn'),
            loading3pids: false,
        });
    }

    _renderEmailField() {
        const { email, newEmail } = this.state;
        const buttonText = email ? 'Change' : 'Add';
        return (
            <div>
                <span className="mx_SettingsTab_subheading">{_t("Email address")}</span>
                <Field id="mx_EmailAddressses_newEmailAddress"
                       type="text"
                       label={_t("Email Address")}
                       autoComplete="off"
                       value={newEmail}
                       onChange={this.handleEmailChange}
                />
                <AccessibleButton
                    disabled={email && (email === newEmail)}
                    onClick={this.handleEmailAdd}
                    kind="primary"
                >
                    {_t(buttonText)}
                </AccessibleButton>
            </div>
        );
    }

    _renderAccountSection() {
        const ChangePassword = sdk.getComponent("views.settings.ChangePassword");
        const PhoneNumbers = sdk.getComponent("views.settings.account.PhoneNumbers");
        const Spinner = sdk.getComponent("views.elements.Spinner");

        let passwordChangeForm = (
            <ChangePassword
                className="mx_GeneralUserSettingsTab_changePassword"
                rowClassName=""
                buttonKind="primary"
                onError={this._onPasswordChangeError}
                onFinished={this._onPasswordChanged} />
        );

        let threepidSection = null;

        // For older homeservers without separate 3PID add and bind methods (MSC2290),
        // we use a combo add with bind option API which requires an identity server to
        // validate 3PID ownership even if we're just adding to the homeserver only.
        // For newer homeservers with separate 3PID add and bind methods (MSC2290),
        // there is no such concern, so we can always show the HS account 3PIDs.
        if (this.state.haveIdServer || this.state.serverSupportsSeparateAddAndBind === true) {
            const msisdns = this.state.loading3pids
                ? <Spinner />
                : <PhoneNumbers
                    msisdns={this.state.msisdns}
                    onMsisdnsChange={this._onMsisdnsChange}
                />;
            threepidSection = <div>
                <span className="mx_SettingsTab_subheading">{_t("Phone numbers")}</span>
                {msisdns}
            </div>;
        } else if (this.state.serverSupportsSeparateAddAndBind === null) {
            threepidSection = <Spinner />;
        }

        let passwordChangeText = _t("Set a new account password...");
        if (!this.state.canChangePassword) {
            // Just don't show anything if you can't do anything.
            passwordChangeText = null;
            passwordChangeForm = null;
        }

        return (
            <div className="mx_SettingsTab_section mx_GeneralUserSettingsTab_accountSection">
                <span className="mx_SettingsTab_subheading">{_t("Account")}</span>
                <p className="mx_SettingsTab_subsectionText">
                    {passwordChangeText}
                </p>
                {passwordChangeForm}
                {this._renderEmailField()}
                {threepidSection}
            </div>
        );
    }

    _renderDiscoverySection() {
        const SetIdServer = sdk.getComponent("views.settings.SetIdServer");

        if (this.state.requiredPolicyInfo.hasTerms) {
            const InlineTermsAgreement = sdk.getComponent("views.terms.InlineTermsAgreement");
            const intro = <span className="mx_SettingsTab_subsectionText">
                {_t(
                    "Agree to the identity server (%(serverName)s) Terms of Service to " +
                    "allow yourself to be discoverable by email address or phone number.",
                    {serverName: this.state.idServerName},
                )}
            </span>;
            return (
                <div>
                    <InlineTermsAgreement
                        policiesAndServicePairs={this.state.requiredPolicyInfo.policiesAndServices}
                        agreedUrls={this.state.requiredPolicyInfo.agreedUrls}
                        onFinished={this.state.requiredPolicyInfo.resolve}
                        introElement={intro}
                    />
                    { /* has its own heading as it includes the current ID server */}
                    <SetIdServer missingTerms={true} />
                </div>
            );
        }

        const PhoneNumbers = sdk.getComponent("views.settings.discovery.PhoneNumbers");
        const Spinner = sdk.getComponent("views.elements.Spinner");

        const msisdns = this.state.loading3pids ? <Spinner /> : <PhoneNumbers msisdns={this.state.msisdns} />;

        const threepidSection = this.state.haveIdServer ? <div className='mx_GeneralUserSettingsTab_discovery'>
            <span className="mx_SettingsTab_subheading">{_t("Phone numbers")}</span>
            {msisdns}
        </div> : null;

        return (
            <div className="mx_SettingsTab_section">
                {threepidSection}
                { /* has its own heading as it includes the current ID server */}
                <SetIdServer />
            </div>
        );
    }

    _renderThemeSection() {
        return null;
    }
}
