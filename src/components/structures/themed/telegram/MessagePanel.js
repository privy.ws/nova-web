/*
Copyright 2016 OpenMarket Ltd
Copyright 2018 New Vector Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.
Copyright 2020 Nova Technology Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import React from 'react';
import * as sdk from 'matrix-react-sdk/src/index';
import PropTypes from 'prop-types';
import {_t} from "matrix-react-sdk/src/languageHandler";
import {textForEvent} from "matrix-react-sdk/src/TextForEvent";
import MessagePanel from 'matrix-react-sdk/src/components/structures/MessagePanel';

const CONTINUATION_MAX_INTERVAL = 5 * 60 * 1000; // 5 minutes
const continuedTypes = ['m.sticker', 'm.room.message'];

const isMembershipChange = (e) => e.getType() === 'm.room.member' || e.getType() === 'm.room.third_party_invite';

/* (almost) stateless UI component which builds the event tiles in the room timeline.
 */
export default class NovaMessagePanel extends MessagePanel {
    static propTypes = {
        ...super.propTypes,
        directChat: PropTypes.bool,
        isBridged: PropTypes.bool,
    };

    constructor(props, context) {
        super(props, context);

        this.state = {
           ...this.state,
        };
        // FIXME hack
        const bridgeInfo = Object.values(props.room?.currentState?.events?.["m.bridge"] || {})?.[0]?.event?.content;
        this.bridgeBot = bridgeInfo?.bridgebot;
        this.bridgeName = bridgeInfo?.protocol.display_name;
        this.notDeliveredRefresh = null;
    }

    _getEventTiles() {
        this.eventNodes = {};

        let i;

        // first figure out which is the last event in the list which we're
        // actually going to show; this allows us to behave slightly
        // differently for the last event in the list. (eg show timestamp)
        //
        // we also need to figure out which is the last event we show which isn't
        // a local echo, to manage the read-marker.
        let lastShownEvent;

        let lastShownNonLocalEchoIndex = -1;
        for (i = this.props.events.length-1; i >= 0; i--) {
            const mxEv = this.props.events[i];
            if (!this._shouldShowEvent(mxEv)) {
                continue;
            }

            if (lastShownEvent === undefined) {
                lastShownEvent = mxEv;
            }

            if (mxEv.status) {
                // this is a local echo
                continue;
            }

            lastShownNonLocalEchoIndex = i;
            break;
        }

        const ret = [];

        let prevEvent = null; // the last event we showed

        this._readReceiptsByEvent = {};
        if (this.props.showReadReceipts) {
            this._readReceiptsByEvent = this._getReadReceiptsByShownEvent();
        }

        let grouper = null;

        for (i = 0; i < this.props.events.length; i++) {
            const mxEv = this.props.events[i];
            const nextEvent = this.props.events[i+1] || null;
            const eventId = mxEv.getId();
            const last = (mxEv === lastShownEvent);

            if (grouper) {
                if (grouper.shouldGroup(mxEv)) {
                    grouper.add(mxEv);
                    continue;
                } else {
                    // not part of group, so get the group tiles, close the
                    // group, and continue like a normal event
                    ret.push(...grouper.getTiles());
                    prevEvent = grouper.getNewPrevEvent();
                    grouper = null;
                }
            }

            for (const Grouper of groupers) {
                if (Grouper.canStartGroup(this, mxEv)) {
                    grouper = new Grouper(this, mxEv, prevEvent, lastShownEvent);
                }
            }
            if (!grouper) {
                const wantTile = this._shouldShowEvent(mxEv);
                if (wantTile) {
                    // make sure we unpack the array returned by _getTilesForEvent,
                    // otherwise react will auto-generate keys and we will end up
                    // replacing all of the DOM elements every time we paginate.
                    ret.push(...this._getTilesForEvent(prevEvent, mxEv, last, nextEvent));
                    prevEvent = mxEv;
                }

                const readMarker = this._readMarkerForEvent(eventId, i >= lastShownNonLocalEchoIndex);
                if (readMarker) ret.push(readMarker);
            }
        }

        if (grouper) {
            ret.push(...grouper.getTiles());
        }

        return ret;
    }

    _getTilesForEvent(prevEvent, mxEv, last, nextEvent) {
        const EventTile = sdk.getComponent('rooms.EventTile');
        const DateSeparator = sdk.getComponent('messages.DateSeparator');
        const ret = [];

        const isEditing = this.props.editState &&
            this.props.editState.getEvent().getId() === mxEv.getId();
        // is this a continuation of the previous message?
        let continuation = false;

        // Some events should appear as continuations from previous events of
        // different types.

        const eventTypeContinues =
            prevEvent !== null &&
            continuedTypes.includes(mxEv.getType()) &&
            continuedTypes.includes(prevEvent.getType());

        // if there is a previous event and it has the same sender as this event
        // and the types are the same/is in continuedTypes and the time between them is <= CONTINUATION_MAX_INTERVAL
        if (prevEvent !== null && prevEvent.sender && mxEv.sender && mxEv.sender.userId === prevEvent.sender.userId &&
            (mxEv.getType() === prevEvent.getType() || eventTypeContinues) &&
            (mxEv.getTs() - prevEvent.getTs() <= CONTINUATION_MAX_INTERVAL)) {
            continuation = true;
        }

        let lastInGroup = false;
        if (nextEvent) {
            const eventTypeContinues = continuedTypes.includes(mxEv.getType()) && continuedTypes.includes(nextEvent.getType());
            if (nextEvent.sender && mxEv.sender && mxEv.sender && mxEv.sender.userId !== nextEvent.sender.userId || !eventTypeContinues || nextEvent.getTs() - mxEv.getTs() >= CONTINUATION_MAX_INTERVAL) {
                lastInGroup = true;
            }
        }
        /*
                // Work out if this is still a continuation, as we are now showing commands
                // and /me messages with their own little avatar. The case of a change of
                // event type (commands) is handled above, but we need to handle the /me
                // messages seperately as they have a msgtype of 'm.emote' but are classed
                // as normal messages
                if (prevEvent !== null && prevEvent.sender && mxEv.sender
                        && mxEv.sender.userId === prevEvent.sender.userId
                        && mxEv.getType() == prevEvent.getType()
                        && prevEvent.getContent().msgtype === 'm.emote') {
                    continuation = false;
                }
        */

        // local echoes have a fake date, which could even be yesterday. Treat them
        // as 'today' for the date separators.
        let ts1 = mxEv.getTs();
        let eventDate = mxEv.getDate();
        if (mxEv.status) {
            eventDate = new Date();
            ts1 = eventDate.getTime();
        }

        // do we need a date separator since the last event?
        if (this._wantsDateSeparator(prevEvent, eventDate)) {
            const dateSeparator = <li key={ts1}><DateSeparator key={ts1} ts={ts1} /></li>;
            ret.push(dateSeparator);
            continuation = false;
        }

        const eventId = mxEv.getId();
        const highlight = (eventId === this.props.highlightedEventId);

        // we can't use local echoes as scroll tokens, because their event IDs change.
        // Local echos have a send "status".
        const scrollToken = mxEv.status ? undefined : eventId;

        const readReceipts = this._readReceiptsByEvent[eventId];
        if (this.bridgeBot && prevEvent
                && this._readReceiptsByUserId[this.bridgeBot]?.lastShownEventId === prevEvent?.getId()) {
            if (this.notDeliveredRefresh) {
                clearTimeout(this.notDeliveredRefresh);
                this.notDeliveredRefresh = null;
            }
            if (mxEv.getTs() + 10 * 1000 < Date.now()) {
                ret.push(<li key="notdelivered">
                    <h2 className="mx_DateSeparator" role="separator" tabIndex={-1}>
                        <div style={{background: "rgba(255, 105, 115, 0.50)"}}>
                            Messages below haven't been delivered to {this.bridgeName}
                        </div>
                    </h2>
                </li>);
            } else {
                this.notDeliveredRefresh = setTimeout(() => {
                    console.log("Re-rendering message panel to update not-delivered warning");
                    this.setState(state => state);
                }, 5100);
            }
        }

        // Dev note: `this._isUnmounting.bind(this)` is important - it ensures that
        // the function is run in the context of this class and not EventTile, therefore
        // ensuring the right `this._mounted` variable is used by read receipts (which
        // don't update their position if we, the MessagePanel, is unmounting).
        ret.push(
            <li key={eventId}
                ref={this._collectEventNode.bind(this, eventId)}
                data-scroll-tokens={scrollToken}
            >
                <EventTile mxEvent={mxEv}
                           continuation={continuation}
                           isRedacted={mxEv.isRedacted()}
                           replacingEventId={mxEv.replacingEventId()}
                           editState={isEditing && this.props.editState}
                           onHeightChanged={this._onHeightChanged}
                           readReceipts={readReceipts}
                           readReceiptMap={this._readReceiptMap}
                           showUrlPreview={this.props.showUrlPreview}
                           checkUnmounting={this._isUnmounting.bind(this)}
                           eventSendStatus={mxEv.getAssociatedStatus()}
                           tileShape={this.props.tileShape}
                           isTwelveHour={this.props.isTwelveHour}
                           permalinkCreator={this.props.permalinkCreator}
                           last={last}
                           lastInGroup={lastInGroup}
                           directChat={this.props.directChat}
                           isBridged={this.props.isBridged}
                           isSelectedEvent={highlight}
                           getRelationsForEvent={this.props.getRelationsForEvent}
                           showReactions={this.props.showReactions}
                />
            </li>,
        );

        return ret;
    }
}
/* Grouper classes determine when events can be grouped together in a summary.
 * Groupers should have the following methods:
 * - canStartGroup (static): determines if a new group should be started with the
 *   given event
 * - shouldGroup: determines if the given event should be added to an existing group
 * - add: adds an event to an existing group (should only be called if shouldGroup
 *   return true)
 * - getTiles: returns the tiles that represent the group
 * - getNewPrevEvent: returns the event that should be used as the new prevEvent
 *   when determining things such as whether a date separator is necessary
 */

// Wrap initial room creation events into an EventListSummary
// Grouping only events sent by the same user that sent the `m.room.create` and only until
// the first non-state event or membership event which is not regarding the sender of the `m.room.create` event
class CreationGrouper {
    static canStartGroup = function(panel, ev) {
        return ev.getType() === "m.room.create";
    };

    constructor(panel, createEvent, prevEvent, lastShownEvent) {
        this.panel = panel;
        this.createEvent = createEvent;
        this.prevEvent = prevEvent;
        this.lastShownEvent = lastShownEvent;
        this.events = [];
        // events that we include in the group but then eject out and place
        // above the group.
        this.ejectedEvents = [];
        this.readMarker = panel._readMarkerForEvent(
            createEvent.getId(),
            createEvent === lastShownEvent,
        );
    }

    shouldGroup(ev) {
        const panel = this.panel;
        const createEvent = this.createEvent;
        if (!panel._shouldShowEvent(ev)) {
            return true;
        }
        if (panel._wantsDateSeparator(this.createEvent, ev.getDate())) {
            return false;
        }
        if (ev.getType() === "m.room.member"
            && (ev.getStateKey() !== createEvent.getSender() || ev.getContent()["membership"] !== "join")) {
            return false;
        }
        if (ev.isState() && ev.getSender() === createEvent.getSender()) {
            return true;
        }
        return false;
    }

    add(ev) {
        const panel = this.panel;
        this.readMarker = this.readMarker || panel._readMarkerForEvent(
            ev.getId(),
            ev === this.lastShownEvent,
        );
        if (!panel._shouldShowEvent(ev)) {
            return;
        }
        if (ev.getType() === "m.room.encryption") {
            this.ejectedEvents.push(ev);
        } else {
            this.events.push(ev);
        }
    }

    getTiles() {
        // If we don't have any events to group, don't even try to group them. The logic
        // below assumes that we have a group of events to deal with, but we might not if
        // the events we were supposed to group were redacted.
        if (!this.events || !this.events.length) return [];

        const DateSeparator = sdk.getComponent('messages.DateSeparator');
        const EventListSummary = sdk.getComponent('views.elements.EventListSummary');

        const panel = this.panel;
        const ret = [];
        const createEvent = this.createEvent;
        const lastShownEvent = this.lastShownEvent;

        if (panel._wantsDateSeparator(this.prevEvent, createEvent.getDate())) {
            const ts = createEvent.getTs();
            ret.push(
                <li key={ts+'~'}><DateSeparator key={ts+'~'} ts={ts} /></li>,
            );
        }

        // If this m.room.create event should be shown (room upgrade) then show it before the summary
        if (panel._shouldShowEvent(createEvent)) {
            // pass in the createEvent as prevEvent as well so no extra DateSeparator is rendered
            ret.push(...panel._getTilesForEvent(createEvent, createEvent, false));
        }

        for (const ejected of this.ejectedEvents) {
            ret.push(...panel._getTilesForEvent(
                createEvent, ejected, createEvent === lastShownEvent,
            ));
        }

        const eventTiles = this.events.map((e) => {
            // In order to prevent DateSeparators from appearing in the expanded form
            // of EventListSummary, render each member event as if the previous
            // one was itself. This way, the timestamp of the previous event === the
            // timestamp of the current event, and no DateSeparator is inserted.
            return panel._getTilesForEvent(e, e, e === lastShownEvent);
        }).reduce((a, b) => a.concat(b), []);
        // Get sender profile from the latest event in the summary as the m.room.create doesn't contain one
        const ev = this.events[this.events.length - 1];
        ret.push(
            <EventListSummary
                key="roomcreationsummary"
                events={this.events}
                onToggle={panel._onHeightChanged} // Update scroll state
                summaryMembers={[ev.sender]}
                summaryText={_t("%(creator)s created and configured the room.", {
                    creator: ev.sender ? ev.sender.name : ev.getSender(),
                })}
            >
                { eventTiles }
            </EventListSummary>,
        );

        if (this.readMarker) {
            ret.push(this.readMarker);
        }

        return ret;
    }

    getNewPrevEvent() {
        return this.createEvent;
    }
}

// Wrap consecutive member events in a ListSummary, ignore if redacted
class MemberGrouper {
    static canStartGroup = function(panel, ev) {
        return panel._shouldShowEvent(ev) && isMembershipChange(ev);
    }

    constructor(panel, ev, prevEvent, lastShownEvent) {
        this.panel = panel;
        this.readMarker = panel._readMarkerForEvent(
            ev.getId(),
            ev === lastShownEvent,
        );
        this.events = [ev];
        this.prevEvent = prevEvent;
        this.lastShownEvent = lastShownEvent;
    }

    shouldGroup(ev) {
        if (this.panel._wantsDateSeparator(this.events[0], ev.getDate())) {
            return false;
        }
        return isMembershipChange(ev);
    }

    add(ev) {
        if (ev.getType() === 'm.room.member') {
            // We'll just double check that it's worth our time to do so, through an
            // ugly hack. If textForEvent returns something, we should group it for
            // rendering but if it doesn't then we'll exclude it.
            const renderText = textForEvent(ev);
            if (!renderText || renderText.trim().length === 0) return; // quietly ignore
        }
        this.readMarker = this.readMarker || this.panel._readMarkerForEvent(
            ev.getId(),
            ev === this.lastShownEvent,
        );
        this.events.push(ev);
    }

    getTiles() {
        // If we don't have any events to group, don't even try to group them. The logic
        // below assumes that we have a group of events to deal with, but we might not if
        // the events we were supposed to group were redacted.
        if (!this.events || !this.events.length) return [];

        const DateSeparator = sdk.getComponent('messages.DateSeparator');
        const MemberEventListSummary = sdk.getComponent('views.elements.MemberEventListSummary');

        const panel = this.panel;
        const lastShownEvent = this.lastShownEvent;
        const ret = [];

        if (panel._wantsDateSeparator(this.prevEvent, this.events[0].getDate())) {
            const ts = this.events[0].getTs();
            ret.push(
                <li key={ts+'~'}><DateSeparator key={ts+'~'} ts={ts} /></li>,
            );
        }

        // Ensure that the key of the MemberEventListSummary does not change with new
        // member events. This will prevent it from being re-created unnecessarily, and
        // instead will allow new props to be provided. In turn, the shouldComponentUpdate
        // method on MELS can be used to prevent unnecessary renderings.
        //
        // Whilst back-paginating with a MELS at the top of the panel, prevEvent will be null,
        // so use the key "membereventlistsummary-initial". Otherwise, use the ID of the first
        // membership event, which will not change during forward pagination.
        const key = "membereventlistsummary-" + (
            this.prevEvent ? this.events[0].getId() : "initial"
        );

        let highlightInMels = false;
        let eventTiles = this.events.map((e) => {
            if (e.getId() === panel.props.highlightedEventId) {
                highlightInMels = true;
            }
            // In order to prevent DateSeparators from appearing in the expanded form
            // of MemberEventListSummary, render each member event as if the previous
            // one was itself. This way, the timestamp of the previous event === the
            // timestamp of the current event, and no DateSeparator is inserted.
            return panel._getTilesForEvent(e, e, e === lastShownEvent);
        }).reduce((a, b) => a.concat(b), []);

        if (eventTiles.length === 0) {
            eventTiles = null;
        }

        ret.push(
            <MemberEventListSummary key={key}
                                    events={this.events}
                                    onToggle={panel._onHeightChanged} // Update scroll state
                                    startExpanded={highlightInMels}
            >
                { eventTiles }
            </MemberEventListSummary>,
        );

        if (this.readMarker) {
            ret.push(this.readMarker);
        }

        return ret;
    }

    getNewPrevEvent() {
        return this.events[0];
    }
}

// all the grouper classes that we use
const groupers = [CreationGrouper, MemberGrouper];
