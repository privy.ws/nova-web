/*
Copyright 2020 Nova Technology Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import React, { createRef } from 'react';
import classNames from 'classnames';
import * as sdk from 'matrix-react-sdk/src';
import dis from 'matrix-react-sdk/src/dispatcher';
import * as Unread from 'matrix-react-sdk/src/Unread';
import * as RoomNotifs from 'matrix-react-sdk/src/RoomNotifs';
import { Key } from 'matrix-react-sdk/src/Keyboard';
import PropTypes from 'prop-types';
import LazyRenderList from 'matrix-react-sdk/src/components/views/elements/LazyRenderList';
import IndicatorScrollbar from 'matrix-react-sdk/src/components/structures/IndicatorScrollbar';

// turn this on for drop & drag console debugging galore
const debug = false;

export default class RoomSubList extends React.PureComponent {
    static debug = debug;

    static propTypes = {
        list: PropTypes.arrayOf(PropTypes.object).isRequired,
        label: PropTypes.string,
        addRoomLabel: PropTypes.string,
        className: PropTypes.string,
        // passed through to RoomTile and used to highlight room with `!` regardless of notifications count
        isInvite: PropTypes.bool,
        startAsHidden: PropTypes.bool,

        showSpinner: PropTypes.bool, // true to show a spinner if 0 elements when expanded
        collapsed: PropTypes.bool, // is LeftPanel collapsed?
        extraTiles: PropTypes.arrayOf(PropTypes.node), // extra elements added beneath tiles,
        forceExpand: PropTypes.bool,
        itemHeight: PropTypes.number.isRequired,
        nestedList: PropTypes.func,
        nestedListHeight: PropTypes.number,
    };

    static defaultProps = {
        extraTiles: [],
        nestedListHeight: 0,
        isInvite: false,
        collapsed: false,
        label: '',
    };

    static getDerivedStateFromProps(props, state) {
        return {
            listLength: props.list.length,
            scrollTop: props.list.length === state.listLength ? state.scrollTop : -(props.nestedListHeight),
        };
    }

    constructor(props, context) {
        super(props, context);

        this.state = {
            hidden: this.props.startAsHidden || false,
            // some values to get LazyRenderList starting
            scrollTop: 0,
            // React 16's getDerivedStateFromProps(props, state) doesn't give the previous props so
            // we have to store the length of the list here so we can see if it's changed or not...
            listLength: null,
        };

        this._subList = createRef();
        this._scroller = createRef();
    }

    componentDidMount() {
        this.dispatcherRef = dis.register(this.onAction);
    }

    componentWillUnmount() {
        dis.unregister(this.dispatcherRef);
    }

    onAction = (payload) => {
        // XXX: Previously RoomList would forceUpdate whenever on_room_read is dispatched,
        // but this is no longer true, so we must do it here (and can apply the small
        // optimisation of checking that we care about the room being read).
        //
        // Ultimately we need to transition to a state pushing flow where something
        // explicitly notifies the components concerned that the notif count for a room
        // has change (e.g. a Flux store).
        if (payload.action === 'on_room_read' &&
            this.props.list.some((r) => r.roomId === payload.roomId)
        ) {
            this.forceUpdate();
        }
    };

    onRoomTileClick = (roomId, ev) => {
        dis.dispatch({
            action: 'view_room',
            room_id: roomId,
            show_room_tile: true,
            clear_search: (ev && (ev.key === Key.ENTER || ev.key === Key.SPACE)),
        });
    };

    _updateSubListCount = () => {
        // Force an update by setting the state to the current state
        // Doing it this way rather than using forceUpdate(), so that the shouldComponentUpdate()
        // method is honoured
        this.setState(this.state);
    };

    makeRoomTile = (room) => {
        const RoomTile = sdk.getComponent('rooms.RoomTile');
        return <RoomTile
            room={room}
            key={room.roomId}
            collapsed={this.props.collapsed || false}
            unread={Unread.doesRoomHaveUnreadMessages(room)}
            highlight={this.props.isInvite || RoomNotifs.getUnreadNotificationCount(room, 'highlight') > 0}
            notificationCount={RoomNotifs.getUnreadNotificationCount(room)}
            isInvite={this.props.isInvite}
            refreshSubList={this._updateSubListCount}
            onClick={this.onRoomTileClick}
        />;
    };

    checkOverflow = () => {
        if (this._scroller.current) {
            this._scroller.current.checkOverflow();
        }
    };

    _onScroll = () => {
        this.setState({scrollTop: this._scroller.current.getScrollTop() - this.props.nestedListHeight});
    };

    render() {
        const len = this.props.list.length + this.props.extraTiles.length;
        // const isHidden = this.state.hidden && !this.props.forceExpand;

        const subListClasses = classNames({
            "mx_RoomSubList": true,
            // "mx_RoomSubList_hidden": len && isCollapsed,
            "mx_RoomSubList_nonEmpty": len,
            [this.props.className]: !!this.props.className,
        });

        let content;
        if (len || this.props.nestedList) {
                content = (
                    <IndicatorScrollbar ref={this._scroller} className="mx_RoomSubList_scroll" onScroll={this._onScroll}>
                        {this.props.nestedList && this.props.nestedList(this._updateSubListCount)}
                        <LazyRenderList
                            scrollTop={this.state.scrollTop}
                            height={this.props.scrollerHeight}
                            itemHeight={this.props.itemHeight}
                            renderItem={ this.makeRoomTile}
                            items={ this.props.list } />
                        {this.props.extraTiles}
                    </IndicatorScrollbar>
                );
        } else {
            if (this.props.showSpinner) {
                const Loader = sdk.getComponent("elements.Spinner");
                content = <Loader />;
            }
        }

        return (
            <div
                ref={this._subList}
                style={{height: `${this.props.scrollerHeight}px`}}
                className={subListClasses}
                role="group"
                aria-label={this.props.label}
                onKeyDown={this.onKeyDown}
            >
                { content }
            </div>
        );
    }
}
