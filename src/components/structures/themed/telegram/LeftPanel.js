/*
Copyright 2015, 2016 OpenMarket Ltd
Copyright 2019 The Matrix.org Foundation C.I.C.
Copyright 2020 Nova Technology Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import React from 'react';
// import PropTypes from 'prop-types';
import classNames from 'classnames';
import * as sdk from 'matrix-react-sdk/src/index';
import * as VectorConferenceHandler from 'matrix-react-sdk/src/VectorConferenceHandler';
import SettingsStore from 'matrix-react-sdk/src/settings/SettingsStore';
import {_t} from "matrix-react-sdk/src/languageHandler";
import LeftPanel from 'matrix-react-sdk/src/components/structures/LeftPanel';

export default class NovaLeftPanel extends LeftPanel {
    static propTypes = {
        ...super.propTypes,
    };

    constructor(props, context) {
        super(props, context);
        this.state = {
            ...this.state,
        };
    }

    render() {
        const RoomList = sdk.getComponent('rooms.RoomList');
        const RoomBreadcrumbs = sdk.getComponent('rooms.RoomBreadcrumbs');
        const TagPanel = sdk.getComponent('structures.TagPanel');
        const CustomRoomTagPanel = sdk.getComponent('structures.CustomRoomTagPanel');
        const TopLeftMenuButton = sdk.getComponent('structures.TopLeftMenuButton');
        const SearchBox = sdk.getComponent('structures.SearchBox');
        const CallPreview = sdk.getComponent('voip.CallPreview');

        // const tagPanelEnabled = SettingsStore.getValue("TagPanel.enableTagPanel");
        const tagPanelEnabled = false;
        let tagPanelContainer;

        const isCustomTagsEnabled = SettingsStore.isFeatureEnabled("feature_custom_tags");

        if (tagPanelEnabled) {
            tagPanelContainer = (<div className="mx_LeftPanel_tagPanelContainer">
                <TagPanel />
                { isCustomTagsEnabled ? <CustomRoomTagPanel /> : undefined }
            </div>);
        }

        const showSearch = this.props.collapsed && !this.state.searchFilter;

        const containerClasses = classNames(
            "mx_LeftPanel_container", "mx_fadable",
            {
                "collapsed": showSearch,
                "mx_LeftPanel_container_hasTagPanel": tagPanelEnabled,
                "mx_fadable_faded": this.props.disabled,
            },
        );

        const searchBox = !showSearch ? (<SearchBox
            className="mx_LeftPanel_filterRooms"
            enableRoomSearchFocus={true}
            enableClearOnViewRoom={true}
            blurredPlaceholder={ _t('Search') }
            placeholder={ _t('Search') }
            onKeyDown={this._onFilterKeyDown}
            onSearch={ this.onSearch }
            onCleared={ this.onSearchCleared }
            onFocus={this._onSearchFocus}
            onBlur={this._onSearchBlur}
            collapsed={showSearch} />) : null;

        let breadcrumbs;
        if (this.state.breadcrumbs) {
            breadcrumbs = (<RoomBreadcrumbs collapsed={this.props.collapsed} />);
        }

        return (
            <div className={containerClasses}>
                { tagPanelContainer }
                <aside className="mx_LeftPanel dark-panel">
                    <div
                        className="mx_LeftPanel_menuRow"
                        onKeyDown={this._onKeyDown}
                        onFocus={this._onFocus}
                        onBlur={this._onBlur}
                    >
                        <TopLeftMenuButton collapsed={this.props.collapsed} />
                        { searchBox }
                    </div>
                    <CallPreview ConferenceHandler={VectorConferenceHandler} />
                    { breadcrumbs }
                    <RoomList
                        onKeyDown={this._onKeyDown}
                        onFocus={this._onFocus}
                        onBlur={this._onBlur}
                        ref={this.collectRoomList}
                        resizeNotifier={this.props.resizeNotifier}
                        collapsed={this.props.collapsed}
                        searchFilter={this.state.searchFilter}
                        ConferenceHandler={VectorConferenceHandler} />
                </aside>
            </div>
        );
    }
}
