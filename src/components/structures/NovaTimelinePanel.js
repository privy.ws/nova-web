/*
Copyright 2016 OpenMarket Ltd
Copyright 2017 Vector Creations Ltd
Copyright 2019 New Vector Ltd
Copyright 2019-2020 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import React from 'react';
import PropTypes from 'prop-types';
import {EventTimeline} from "matrix-js-sdk";
import {MatrixClientPeg} from "matrix-react-sdk/src/MatrixClientPeg";
import TimelinePanel from 'matrix-react-sdk/src/components/structures/TimelinePanel';
import * as sdk from "matrix-react-sdk/src/index";

export default class NovaTimelinePanel extends TimelinePanel {
    static replaces = 'TimelinePanel';

    static propTypes = {
        ...TimelinePanel.propTypes,
        directChat: PropTypes.bool,
        isBridged: PropTypes.bool,
    };

    constructor(props) {
        super(props);
    }

    render() {
        const MessagePanel = sdk.getComponent("structures.MessagePanel");
        const Loader = sdk.getComponent("elements.Spinner");

        // just show a spinner while the timeline loads.
        //
        // put it in a div of the right class (mx_RoomView_messagePanel) so
        // that the order in the roomview flexbox is correct, and
        // mx_RoomView_messageListWrapper to position the inner div in the
        // right place.
        //
        // Note that the click-on-search-result functionality relies on the
        // fact that the messagePanel is hidden while the timeline reloads,
        // but that the RoomHeader (complete with search term) continues to
        // exist.
        if (this.state.timelineLoading) {
            return (
                <div className="mx_RoomView_messagePanelSpinner">
                    <Loader />
                </div>
            );
        }

        if (this.state.events.length == 0 && !this.state.canBackPaginate && this.props.empty) {
            return (
                <div className={this.props.className + " mx_RoomView_messageListWrapper"}>
                    <div className="mx_RoomView_empty">{this.props.empty}</div>
                </div>
            );
        }

        // give the messagepanel a stickybottom if we're at the end of the
        // live timeline, so that the arrival of new events triggers a
        // scroll.
        //
        // Make sure that stickyBottom is *false* if we can paginate
        // forwards, otherwise if somebody hits the bottom of the loaded
        // events when viewing historical messages, we get stuck in a loop
        // of paginating our way through the entire history of the room.
        const stickyBottom = !this._timelineWindow.canPaginate(EventTimeline.FORWARDS);

        // If the state is PREPARED or CATCHUP, we're still waiting for the js-sdk to sync with
        // the HS and fetch the latest events, so we are effectively forward paginating.
        const forwardPaginating = (
            this.state.forwardPaginating ||
            ['PREPARED', 'CATCHUP'].includes(this.state.clientSyncState)
        );
        const events = this.state.firstVisibleEventIndex
            ? this.state.events.slice(this.state.firstVisibleEventIndex)
            : this.state.events;
        return (
            <MessagePanel
                ref={this._messagePanel}
                room={this.props.timelineSet.room}
                permalinkCreator={this.props.permalinkCreator}
                hidden={this.props.hidden}
                backPaginating={this.state.backPaginating}
                forwardPaginating={forwardPaginating}
                events={events}
                highlightedEventId={this.props.highlightedEventId}
                readMarkerEventId={this.state.readMarkerEventId}
                readMarkerVisible={this.state.readMarkerVisible}
                suppressFirstDateSeparator={this.state.canBackPaginate}
                showUrlPreview={this.props.showUrlPreview}
                showReadReceipts={this.props.showReadReceipts}
                ourUserId={MatrixClientPeg.get().credentials.userId}
                stickyBottom={stickyBottom}
                onScroll={this.onMessageListScroll}
                onFillRequest={this.onMessageListFillRequest}
                onUnfillRequest={this.onMessageListUnfillRequest}
                isTwelveHour={this.state.isTwelveHour}
                alwaysShowTimestamps={this.state.alwaysShowTimestamps}
                className={this.props.className}
                tileShape={this.props.tileShape}
                resizeNotifier={this.props.resizeNotifier}
                getRelationsForEvent={this.getRelationsForEvent}
                editState={this.state.editState}
                showReactions={this.props.showReactions}
                directChat={this.props.directChat}
                isBridged={this.props.isBridged}
            />
        );
    }
}
