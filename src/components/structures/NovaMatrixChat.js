/*
Copyright 2020 Nova Technology Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import React from 'react';
// import PropTypes from 'prop-types';
import * as Matrix from "matrix-js-sdk";
// focus-visible is a Polyfill for the :focus-visible CSS pseudo-attribute used by _AccessibleButton.scss
import 'focus-visible';
// what-input helps improve keyboard accessibility
import 'what-input';
import { MatrixClientPeg } from "matrix-react-sdk/src/MatrixClientPeg";
import SdkConfig from "matrix-react-sdk/src/SdkConfig";
import dis from "matrix-react-sdk/src/dispatcher";

import Modal from "matrix-react-sdk/src/Modal";
import * as sdk from 'matrix-react-sdk/src/index';
import * as Lifecycle from 'matrix-react-sdk/src/Lifecycle';
// LifecycleStore is not used but does listen to and dispatch actions
import 'matrix-react-sdk/src/stores/LifecycleStore';

import { _t } from 'matrix-react-sdk/src/languageHandler';
import SettingsStore, {SettingLevel} from "matrix-react-sdk/src/settings/SettingsStore";
import { messageForSyncError } from 'matrix-react-sdk/src/utils/ErrorUtils';
import { isCryptoAvailable } from 'matrix-js-sdk/src/crypto';
import MatrixChat, { Views } from 'matrix-react-sdk/src/components/structures/MatrixChat';
import Mixpanel from "../../utils/Mixpanel";
import RoomsLazyLoader from '../../utils/RoomsLazyLoader';
import { boundMethod } from 'autobind-decorator';

/** constants for MatrixChat.state.view */

// Actions that are redirected through the onboarding process prior to being
// re-dispatched. NOTE: some actions are non-trivial and would require
// re-factoring to be included in this list in future.
const ONBOARDING_FLOW_STARTERS = [
    'view_user_settings',
    'view_create_chat',
    'view_create_room',
    'view_create_group',
];

export default class NovaMatrixChat extends MatrixChat {
    static statics = {
        Views: Views,
    };

    static propTypes = {
        ...super.propTypes,
    };

    static defaultProps = {
        ...super.defaultProps,
    };

    constructor(props, context) {
        super(props, context);
        this.state = {
            ...this.state,
            trackingEnable: true,
        };
    }

    @boundMethod
    onAction(payload) {
        // Start the onboarding process for certain actions
        if (MatrixClientPeg.get() && MatrixClientPeg.get().isGuest() &&
            ONBOARDING_FLOW_STARTERS.includes(payload.action)
        ) {
            // This will cause `payload` to be dispatched later, once a
            // sync has reached the "prepared" state. Setting a matrix ID
            // will cause a full login and sync and finally the deferred
            // action will be dispatched.
            dis.dispatch({
                action: 'do_after_sync_prepared',
                deferred_action: payload,
            });
            dis.dispatch({action: 'require_registration'});
            return;
        }

        let handled = false;

        switch (payload.action) {
            case 'MatrixActions.sync':
                // Initialise lazy singleton after initial sync
                if (!(this.firstSyncComplete)) {
                    break;
                }
                RoomsLazyLoader.sync(payload);
                handled = true;
                break;
            case 'on_logged_in':
                if (
                    !Lifecycle.isSoftLogout() &&
                    this.state.view !== Views.LOGIN &&
                    this.state.view !== Views.ORIGINAL_LOGIN &&
                    this.state.view !== Views.REGISTER &&
                    this.state.view !== Views.COMPLETE_SECURITY &&
                    this.state.view !== Views.E2E_SETUP
                ) {
                    this.onLoggedIn();
                    handled = true;
                }
                break;
            case 'logout':
                Mixpanel.logout();
                break;
            case 'view_room':
                RoomsLazyLoader.markRoomAsLoaded(payload.room_id);
                break;
            case 'view_chat_networks': {
                const ChatNetworksDialog = sdk.getComponent("dialogs.ChatNetworksDialog");
                Modal.createTrackedDialog('Set Up Chat Networks', '', ChatNetworksDialog);
                dis.dispatch({ action: 'track_event', message: 'Open Chat Networks modal' });

                // View the welcome or home page if we need something to look at
                this.viewSomethingBehindModal();
                handled = true;
                break;
            }
            case 'view_tag_manager': {
                const TagManagerDialog = sdk.getComponent("dialogs.TagManagerDialog");
                Modal.createTrackedDialog('Tag manager', '', TagManagerDialog);
                dis.dispatch({ action: 'track_event', message: 'Open Tag Manager modal' });
                // View the welcome or home page if we need something to look at
                this.viewSomethingBehindModal();
                handled = true;
                break;
            }
            case 'view_welcome_page':
                // Owerwrited to open by login page default
                this.viewLoginPage();
                handled = true;
                break;
            case 'view_original_login_page':
                this.viewOriginalLoginPage();
                handled = true;
                break;
            case 'accept_cookies':
                SettingsStore.setValue("analyticsOptIn", null, SettingLevel.DEVICE, true);
                SettingsStore.setValue("showCookieBar", null, SettingLevel.DEVICE, false);
                this.setState({
                    showCookieBar: false,
                    trackingEnable: true,
                });
                handled = true;
                break;
            case 'reject_cookies':
                SettingsStore.setValue("analyticsOptIn", null, SettingLevel.DEVICE, false);
                SettingsStore.setValue("showCookieBar", null, SettingLevel.DEVICE, false);

                this.setState({
                    // Tracking always enabled for now
                    showCookieBar: false,
                    // trackingEnable: false,
                });
                handled = true;
                break;
            case 'track_event':
                if (this.state.trackingEnable) {
                    Mixpanel.track(payload.message);
                }
                handled = true;
                break;
        }
        if (!handled) {
            super.onAction(payload);
        }
    }

    // Add default login view
    viewOriginalLoginPage() {
        this.setStateForNewView({
            view: Views.ORIGINAL_LOGIN,
        });
        this.notifyNewScreen('original_login');
    }

    showScreen(screen, params) {
        if (screen === 'original_login') {
            dis.dispatch({
                action: 'view_original_login_page',
                params: params,
            });
        } else {
            super.showScreen(screen, params);
        }
    }

    // Overwrited to prevent collapse left panel on window resize
    @boundMethod
    handleResize(e) {
        this.state.resizeNotifier.notifyWindowResized();
    }

    // Overwrited default title brand
    setPageSubtitle(subtitle='') {
        if (this.state.currentRoomId) {
            const client = MatrixClientPeg.get();
            const room = client && client.getRoom(this.state.currentRoomId);
            if (room) {
                subtitle = `${this.subTitleStatus} | ${ room.name } ${subtitle}`;
            }
        } else {
            subtitle = `${this.subTitleStatus} ${subtitle}`;
        }
        document.title = `${SdkConfig.get().brand || 'NovaChat'} ${subtitle}`;
    }

    // Overwrited to identify login by Mixpanel
    @boundMethod
    async onUserCompletedLoginFlow(credentials, password) {
        this._accountPassword = password;
        // self-destruct the password after 5mins
        if (this._accountPasswordTimer !== null) clearTimeout(this._accountPasswordTimer);
        this._accountPasswordTimer = setTimeout(() => {
            this._accountPassword = null;
            this._accountPasswordTimer = null;
        }, 60 * 5 * 1000);

        // Wait for the client to be logged in (but not started)
        // which is enough to ask the server about account data.
        const loggedIn = new Promise(resolve => {
            const actionHandlerRef = dis.register(payload => {
                if (payload.action !== "on_logged_in") {
                    return;
                }
                dis.unregister(actionHandlerRef);
                resolve();
            });
        });

        // Create and start the client in the background
        const setLoggedInPromise = Lifecycle.setLoggedIn(credentials);
        await loggedIn;

        const cli = MatrixClientPeg.get();

        Mixpanel.identifyLogin(cli.getUserId());
        // We're checking `isCryptoAvailable` here instead of `isCryptoEnabled`
        // because the client hasn't been started yet.
        const cryptoAvailable = isCryptoAvailable();
        if (!cryptoAvailable) {
            this.onLoggedIn();
        }

        this.setState({ pendingInitialSync: true });
        await this.firstSyncPromise.promise;

        if (!cryptoAvailable) {
            this.setState({ pendingInitialSync: false });
            return setLoggedInPromise;
        }

        // Test for the master cross-signing key in SSSS as a quick proxy for
        // whether cross-signing has been set up on the account.
        const masterKeyInStorage = !!cli.getAccountData("m.cross_signing.master");
        if (masterKeyInStorage) {
            // Auto-enable cross-signing for the new session when key found in
            // secret storage.
            SettingsStore.setValue("feature_cross_signing", null, SettingLevel.DEVICE, true);
            this.setStateForNewView({ view: Views.COMPLETE_SECURITY });
        } else if (
            SettingsStore.getValue("feature_cross_signing") &&
            await cli.doesServerSupportUnstableFeature("org.matrix.e2e_cross_signing")
        ) {
            // This will only work if the feature is set to 'enable' in the config,
            // since it's too early in the lifecycle for users to have turned the
            // labs flag on.
            this.setStateForNewView({ view: Views.E2E_SETUP });
        } else {
            this.onLoggedIn();
        }
        this.setState({ pendingInitialSync: false });

        return setLoggedInPromise;
    }

    render() {
        let fragmentAfterLogin = "";
        if (this.props.initialScreenAfterLogin) {
            fragmentAfterLogin = `/${this.props.initialScreenAfterLogin.screen}`;
        }

        let view;

        if (this.state.view === Views.LOADING) {
            const Spinner = sdk.getComponent('elements.Spinner');
            view = (
                <div className="mx_MatrixChat_splash">
                    <Spinner />
                </div>
            );
        } else if (this.state.view === Views.COMPLETE_SECURITY) {
            const CompleteSecurity = sdk.getComponent('structures.auth.CompleteSecurity');
            view = (
                <CompleteSecurity
                    onFinished={this.onCompleteSecurityE2eSetupFinished}
                />
            );
        } else if (this.state.view === Views.E2E_SETUP) {
            const E2eSetup = sdk.getComponent('structures.auth.E2eSetup');
            view = (
                <E2eSetup
                    onFinished={this.onCompleteSecurityE2eSetupFinished}
                    accountPassword={this._accountPassword}
                />
            );
        } else if (this.state.view === Views.POST_REGISTRATION) {
            // needs to be before normal PageTypes as you are logged in technically
            const PostRegistration = sdk.getComponent('structures.auth.PostRegistration');
            view = (
                <PostRegistration
                    onComplete={this.onFinishPostRegistration} />
            );
        } else if (this.state.view === Views.LOGGED_IN) {
            // store errors stop the client syncing and require user intervention, so we'll
            // be showing a dialog. Don't show anything else.
            const isStoreError = this.state.syncError && this.state.syncError instanceof Matrix.InvalidStoreError;

            // `ready` and `view==LOGGED_IN` may be set before `page_type` (because the
            // latter is set via the dispatcher). If we don't yet have a `page_type`,
            // keep showing the spinner for now.
            if (this.state.ready && this.state.page_type && !isStoreError) {
                /* for now, we stuff the entirety of our props and state into the LoggedInView.
                 * we should go through and figure out what we actually need to pass down, as well
                 * as using something like redux to avoid having a billion bits of state kicking around.
                 */
                const LoggedInView = sdk.getComponent('structures.LoggedInView');
                view = (
                    <LoggedInView
                        {...this.props}
                        {...this.state}
                        ref={this.loggedInView}
                        matrixClient={MatrixClientPeg.get()}
                        onRoomCreated={this.onRoomCreated}
                        onCloseAllSettings={this.onCloseAllSettings}
                        onRegistered={this.onRegistered}
                        currentRoomId={this.state.currentRoomId}
                        showCookieBar={this.state.showCookieBar}
                    />
                );
            } else {
                // we think we are logged in, but are still waiting for the /sync to complete
                const Spinner = sdk.getComponent('elements.Spinner');
                let errorBox;
                if (this.state.syncError && !isStoreError) {
                    errorBox = <div className="mx_MatrixChat_syncError">
                        {messageForSyncError(this.state.syncError)}
                    </div>;
                }
                view = (
                    <div className="mx_MatrixChat_splash">
                        {errorBox}
                        <Spinner />
                        <a href="#" className="mx_MatrixChat_splashButtons" onClick={this.onLogoutClick}>
                            {_t('Logout')}
                        </a>
                    </div>
                );
            }
        } else if (this.state.view === Views.WELCOME) {
            const Welcome = sdk.getComponent('auth.Welcome');
            view = <Welcome {...this.getServerProperties()} />;
        } else if (this.state.view === Views.REGISTER) {
            const Registration = sdk.getComponent('structures.auth.Registration');
            view = (
                <Registration
                    clientSecret={this.state.register_client_secret}
                    sessionId={this.state.register_session_id}
                    idSid={this.state.register_id_sid}
                    email={this.props.startingFragmentQueryParams.email}
                    brand={this.props.config.brand}
                    makeRegistrationUrl={this.makeRegistrationUrl}
                    onLoggedIn={this.onRegisterFlowComplete}
                    onLoginClick={this.onLoginClick}
                    onServerConfigChange={this.onServerConfigChange}
                    defaultDeviceDisplayName={this.props.defaultDeviceDisplayName}
                    {...this.getServerProperties()}
                />
            );
        } else if (this.state.view === Views.FORGOT_PASSWORD) {
            const ForgotPassword = sdk.getComponent('structures.auth.ForgotPassword');
            view = (
                <ForgotPassword
                    onComplete={this.onLoginClick}
                    onLoginClick={this.onLoginClick}
                    onServerConfigChange={this.onServerConfigChange}
                    {...this.getServerProperties()}
                />
            );
        } else if (this.state.view === Views.ORIGINAL_LOGIN) {
            const Login = sdk.getComponent(`structures.auth.Login`);
            view = (
                <Login
                    isSyncing={this.state.pendingInitialSync}
                    onLoggedIn={this.onUserCompletedLoginFlow}
                    fallbackHsUrl={this.getFallbackHsUrl()}
                    defaultDeviceDisplayName={this.props.defaultDeviceDisplayName}
                    onServerConfigChange={this.onServerConfigChange}
                    fragmentAfterLogin={fragmentAfterLogin}
                    {...this.getServerProperties()}
                />
            );
        } else if (this.state.view === Views.LOGIN) {
            const Login = sdk.getComponent(`structures.auth.NovaLogin`);
            view = (
                <Login
                    isSyncing={this.state.pendingInitialSync}
                    onLoggedIn={this.onUserCompletedLoginFlow}
                    fallbackHsUrl={this.getFallbackHsUrl()}
                    defaultDeviceDisplayName={this.props.defaultDeviceDisplayName}
                    onServerConfigChange={this.onServerConfigChange}
                    fragmentAfterLogin={fragmentAfterLogin}
                    {...this.getServerProperties()}
                />
            );
        } else if (this.state.view === Views.SOFT_LOGOUT) {
            const SoftLogout = sdk.getComponent('structures.auth.SoftLogout');
            view = (
                <SoftLogout
                    realQueryParams={this.props.realQueryParams}
                    onTokenLoginCompleted={this.props.onTokenLoginCompleted}
                    fragmentAfterLogin={fragmentAfterLogin}
                />
            );
        } else {
            console.error(`Unknown view ${this.state.view}`);
        }

        const ErrorBoundary = sdk.getComponent('elements.ErrorBoundary');
        return <ErrorBoundary>
            {view}
        </ErrorBoundary>;
    }
}
