/*
Copyright 2020 Nova Technology Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// eslint-disable-next-line no-unused-vars
import React from 'react';
import { Key, isOnlyCtrlOrCmdKeyEvent } from 'matrix-react-sdk/src/Keyboard';
import dis from 'matrix-react-sdk/src/dispatcher';
import SettingsStore from "matrix-react-sdk/src/settings/SettingsStore";
import * as Unread from 'matrix-react-sdk/src/Unread';
import {Resizer, CollapseDistributor} from 'matrix-react-sdk/src/resizer';
import LoggedInView from "matrix-react-sdk/src/components/structures/LoggedInView";

/**
 * This is what our MatrixChat shows when we are logged in. The precise view is
 * determined by the page_type property.
 *
 * Currently it's very tightly coupled with MatrixChat. We should try to do
 * something about that.
 *
 * Components mounted below us can access the matrix client via the react context.
 */

export default class NovaLoggedInView extends LoggedInView {
    static replaces = 'LoggedInView';

    static propTypes = {
        ...super.propTypes,
    };

    static defaultProps = {
        ...super.defaultProps,
    };

    constructor(props, context) {
        super(props, context);

        this.state = {
            ...this.state,
            // leftPanelMinWidth: SettingsStore.getValue("TagPanel.enableTagPanel") ? 260 : 190,
            leftPanelMinWidth: false ? 260 : 190,
        };
    }

    _openRecentUnreadRoom() {
        const unreadRooms = this._matrixClient.getRooms()
            .filter(room => !Object.keys(room.tags).includes('m.lowpriority'))
            .filter((room) => Unread.doesRoomHaveUnreadMessages(room))
            .map((room) => {
                const lastMxEvent = room.timeline[room.timeline.length - 1];
                const lastEventTimestamp = lastMxEvent ? lastMxEvent.event.origin_server_ts : 0;
                return {
                    roomId: room.roomId,
                    lastEventTimestamp,
                };
            })
            .sort((a, b) => b.lastEventTimestamp - a.lastEventTimestamp);

        if (!unreadRooms.length) return;

        const { roomId } = unreadRooms[0];
        dis.dispatch({
            action: 'view_room',
            room_id: roomId,
        });
    }

    // Overwritten to control collapse width
    _createResizer() {
        const classNames = {
            handle: "mx_ResizeHandle",
            vertical: "mx_ResizeHandle_vertical",
            reverse: "mx_ResizeHandle_reverse",
        };
        const tagPanelWidth = SettingsStore.getValue("TagPanel.enableTagPanel") ? 70 : 0;
        const collapseConfig = {
            toggleSize: ((this.state.leftPanelMinWidth - tagPanelWidth) / 2 ) + tagPanelWidth,
            onCollapsed: (collapsed) => {
                if (collapsed) {
                    dis.dispatch({action: "hide_left_panel"}, true);
                    window.localStorage.setItem("mx_lhs_size", '0');
                } else {
                    dis.dispatch({action: "show_left_panel"}, true);
                }
            },
            onResized: (size) => {
                window.localStorage.setItem("mx_lhs_size", '' + size);
                this.props.resizeNotifier.notifyLeftHandleResized();
            },
        };
        const resizer = new Resizer(
            this._resizeContainer.current,
            CollapseDistributor,
            collapseConfig);
        resizer.setClassNames(classNames);
        return resizer;
    }

    _loadResizerPreferences() {
        let lhsSize = parseInt(window.localStorage.getItem("mx_lhs_size"), 10);
        if (isNaN(lhsSize)) {
            lhsSize = this.state.leftPanelMinWidth;
        }
        this.resizer.forHandleAt(0).resize(lhsSize);
    }

    _onKeyDown(ev) {
        const ctrlCmdOnly = isOnlyCtrlOrCmdKeyEvent(ev);

        let handled = false;

        switch (ev.key) {
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
                if (ctrlCmdOnly) {
                    dis.dispatch({
                        action: 'select_tag',
                        tagIndex: Number(ev.key),
                    });
                    handled = true;
                }
                break;
            case Key.U:
                if (ctrlCmdOnly) {
                    this._openRecentUnreadRoom();
                    handled = true;
                }
                break;
            case Key.K:
                if (ctrlCmdOnly) {
                    dis.dispatch({
                        action: 'focus_room_filter',
                    });
                    handled = true;
                }
                break;
        }

        if (handled) {
            ev.stopPropagation();
            ev.preventDefault();
        } else {
           super._onKeyDown(ev);
        }
    }
}
