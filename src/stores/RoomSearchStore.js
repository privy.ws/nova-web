import dis from 'matrix-react-sdk/src/dispatcher';
import { Store } from 'flux/utils';
import FussySearch from '../utils/FussySearch';

const INITIAL_STATE = {
    roomsIdsBySearchString: {},
    normalizedStrings: [],
};

const memoResults = new Map();

class RoomSearchStoreStore extends Store {
    constructor() {
        super(dis);
        this._fussySearch = FussySearch;
        this.needUpdate = false;
        this._state = { ...INITIAL_STATE };
    }

    _setState(newState) {
        // If values haven't changed, there's nothing to do.
        // This only tries a shallow comparison, so unchanged objects will slip
        // through, but that's probably okay for now.
        let stateChanged = false;
        for (const key of Object.keys(newState)) {
            if (this._state[key] !== newState[key]) {
                stateChanged = true;
                break;
            }
        }
        if (!stateChanged) {
            return;
        }
        this._state = Object.assign(this._state, newState);
    }

    __onDispatch(payload) {
        switch (payload.action) {
            // Initialise state after initial sync
            case 'MatrixActions.sync':
                // eslint-disable-next-line no-case-declarations
                const isInitialSync = payload.prevState !== 'PREPARED' && payload.state === 'PREPARED';
                if (!isInitialSync) {
                    break;
                }
                this._generateInitialSearchMap(payload.matrixClient.getRooms());
                this._inited = true;
                break;
            case 'MatrixActions.Room.myMembership':
                if (this._inited) {
                    this._updateSearchMapOnMyMembership(payload.room, payload.membership);
                }
                break;
            case 'MatrixActions.Room.receipt':
                if (this._inited) {
                    this._updateSearchMapOnReceipt(payload.room);
                }
                break;
            case 'on_logged_out':
                this.reset();
                break;
        }
    }

    reset() {
        this._state = Object.assign({}, INITIAL_STATE);
        memoResults.clear();
    }

    _normalizeStr(str) {
        return str.toLowerCase().trim();
    }

    _onRoomNameChange(room, event) {
        const content = event.getContent();

        const oldName = this._normalizeStr(room.name);
        const newName = this._normalizeStr(content.name);

        const newNormalizedStrings = [
            ...this._state.normalizedStrings.filter(str => str !== oldName),
            newName,
        ];

        this._setState({
            roomsIdsBySearchString: {
                ...this._state.roomsIdsBySearchString,
                [newName]: this._state.roomsIdsBySearchString[oldName] || [room.roomId],
                [oldName]: [],
            },
            normalizedStrings: newNormalizedStrings,
        });
    }

    _onMemberAdd(room, event) {
        const content = event.getContent();

        const { displayname } = content;
        const name = this._normalizeStr(displayname);

        const newNormalizedStrings = this._state.normalizedStrings.includes(name) ?
            this._state.normalizedStrings : [...this._state.normalizedStrings, name];

        const roomsIds = this._state.roomsIdsBySearchString[name] || [];
        this._setState({
            normalizedStrings: newNormalizedStrings,
            roomsIdsBySearchString: {
                ...this._state.roomsIdsBySearchString,
                [name]: [...roomsIds, room.roomId],
            },
        });
    }

    _onMemberLeave(room, event) {
        const content = event.getContent();
        const { displayname } = content;
        const name = this._normalizeStr(displayname);

        const roomsIds = this._state.roomsIdsBySearchString[name] || [];
        this._setState({
            roomsIdsBySearchString: {
                ...this._state.roomsIdsBySearchString,
                [name]: roomsIds.filter(rId => rId !== room.roomId),
            },
        });
    }

    _updateSearchMapOnReceipt(room) {
        if (!room) return;
        const lastEvent = room.timeline[room.timeline.length - 1];
        const type = lastEvent.getType();
        const content = lastEvent.getContent();

        if (type !== 'm.room.name' && type !== 'm.room.member') return;

        if (type === 'm.room.name') {
            this._onRoomNameChange(room, lastEvent);
        } else {
            const isMe = lastEvent.getStateKey() === room.myUserId;
            if (isMe) {
                return;
            }
            const { membership } = content;
            const isAdded = membership === 'join' || membership === 'invite';
            isAdded ? this._onMemberAdd(room, lastEvent) : this._onMemberLeave(room, lastEvent);
        }

        memoResults.clear();
        this._fussySearch.setDictionary(this._state.normalizedStrings);
    }

    _updateSearchMapOnMyMembership(room, membership) {
        const { name: roomName, myUserId, currentState: { roomId, members } } = room;
        const normalizedRoomName = this._normalizeStr(roomName);

        const newRoomsIdsBySearchString = { ...this._state.roomsIdsBySearchString };
        let newNormalizedStrings = this._state.normalizedStrings.slice();
        let needHandleRoomName = true;

        if (membership === 'join' || membership === 'invite') {
            Object.keys(members).forEach((userId) => {
                const { name: userName, membership } = members[userId];

                if (userId === myUserId || membership !== 'join') return;

                const normalizedUserName = this._normalizeStr(userName);

                if (normalizedRoomName === normalizedUserName) {
                    needHandleRoomName = false;
                }

                const prevArr = newRoomsIdsBySearchString[normalizedUserName] || [];
                newRoomsIdsBySearchString[normalizedUserName] = [...prevArr, roomId];

                if (!newNormalizedStrings.includes(normalizedUserName)) {
                    newNormalizedStrings.push(normalizedUserName);
                }
            });
            if (needHandleRoomName) {
                const prevArr = newRoomsIdsBySearchString[normalizedRoomName] || [];
                newRoomsIdsBySearchString[normalizedRoomName] = [...prevArr, roomId];
                if (!newNormalizedStrings.includes(normalizedRoomName)) {
                    newNormalizedStrings.push(normalizedRoomName);
                }
            }
        } else {
            const strToRemove = [normalizedRoomName];
            Object.keys(members).forEach((userId) => {
                const { name: userName, membership } = members[userId];

                if (userId === myUserId || membership !== 'join') return;

                const normalizedUserName = this._normalizeStr(userName);
                const prevArr = newRoomsIdsBySearchString[normalizedUserName] || [];
                newRoomsIdsBySearchString[normalizedUserName] = prevArr.filter(rId => rId !==roomId);

                if (!newRoomsIdsBySearchString[normalizedUserName].length) {
                    strToRemove.push(normalizedUserName);
                }
            });
            newNormalizedStrings = newNormalizedStrings.filter(str => !strToRemove.includes(str));
        }

        this._setState({
            roomsIdsBySearchString: newRoomsIdsBySearchString,
            normalizedStrings: newNormalizedStrings,
        });

        memoResults.clear();
        this._fussySearch.setDictionary(this._state.normalizedStrings);
    }

    _generateInitialSearchMap(rooms) {
        const roomsIdsBySearchString = {};
        const normalizedStrings = [];

        rooms.forEach((room) => {
            const { name: roomName, myUserId, currentState: { roomId, members } } = room;

            const normalizedRoomName = this._normalizeStr(roomName);

            let needAddRoomName = true;
            Object.keys(members).forEach((userId) => {
                const { name: userName, membership } = members[userId];

                if (userId === myUserId || membership !== 'join') return;

                const normalizedUserName = this._normalizeStr(userName);

                if (normalizedRoomName === normalizedUserName) {
                    needAddRoomName = false;
                }

                const prevArr = roomsIdsBySearchString[normalizedUserName] || [];
                roomsIdsBySearchString[normalizedUserName] = [...prevArr, roomId];

                if (!normalizedStrings.includes(normalizedUserName)) {
                    normalizedStrings.push(normalizedUserName);
                }
            });

            if (needAddRoomName) {
                const prevArr = roomsIdsBySearchString[normalizedRoomName] || [];
                roomsIdsBySearchString[normalizedRoomName] = [...prevArr, roomId];
                if (!normalizedStrings.includes(normalizedRoomName)) {
                    normalizedStrings.push(normalizedRoomName);
                }
            }
        });
        this._fussySearch.setDictionary(normalizedStrings);
        this._setState({ roomsIdsBySearchString, normalizedStrings });
    }

    searchRooms(searchStr) {
        const normalizedStr = this._normalizeStr(searchStr);
        if (memoResults.has(normalizedStr)) {
            return memoResults.get(normalizedStr);
        }
        const resultStrings = this._fussySearch.search(normalizedStr);

        const { roomsIdsBySearchString } = this._state;

        const roomsIds = resultStrings.reduce((acc, str) => {
            return [...acc, ...roomsIdsBySearchString[str]];
        }, []);

        memoResults.set(normalizedStr, roomsIds);
        return roomsIds;
    }
}

let singletonRoomSearchStore = null;
if (!singletonRoomSearchStore) {
    singletonRoomSearchStore = new RoomSearchStoreStore();
}

export default singletonRoomSearchStore;
