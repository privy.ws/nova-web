#!/bin/bash

JS_HASH=$(cd node_modules/matrix-js-sdk; git rev-parse --short=12 HEAD)
REACT_HASH=$(cd node_modules/matrix-react-sdk; git rev-parse --short=12 HEAD)
RIOT_HASH=$(git rev-parse --short=12 HEAD)

cat <<EOF > webapp/nova-config.json
{"app_hash": "$RIOT_HASH-$REACT_HASH-$JS_HASH"}
EOF
